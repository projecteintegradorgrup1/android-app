package dam.android.appproyecto.Activities

import android.content.Intent
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import dam.android.appproyecto.R
import dam.android.appproyecto.model.Reports
import dam.android.appproyecto.Adapters.ReportsAdapter
import kotlinx.android.synthetic.main.activity_reports_asistente.*

class ReportsAsistente : AppCompatActivity() {




    private var arraylistOfReports: ArrayList<Reports> =  arrayListOf()

    private val myImageList = intArrayOf(R.drawable.ic_accessible_black_24dp, R.drawable.ic_warning_black_24dp, R.drawable.ic_local_hospital_black_24dp, R.drawable.ic_assignment_black_24dp,R.drawable.ic_phone_in_talk_black_24dp)
    private val myReportInHTML = arrayOf("http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/avisos1.html?j_username=grup1&j_password=Grupo-427",
            "http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/dependiene.html?j_username=grup1&j_password=Grupo-427",
            "http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/medicos.html?j_username=grup1&j_password=Grupo-427",
            "http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/alarmas.html?j_username=grup1&j_password=Grupo-427")
    private val myReportInPDF = arrayOf("http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/avisos1.pdf?j_username=grup1&j_password=Grupo-427",
            "http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/dependiene.pdf?j_username=grup1&j_password=Grupo-427",
            "http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/medicos.pdf?j_username=grup1&j_password=Grupo-427",
            "http://149.202.8.235:8080/jasperserver/rest_v2/reports/reports/grup1/alarmas.pdf?j_username=grup1&j_password=Grupo-427")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reports_asistente)


        val myImageNameList = resources.getStringArray(R.array.titlesOfReports)
        val mySubtitleList = resources.getStringArray(R.array.subTitlesOfReports)

        val informe = Reports((myImageNameList[0]).toString(), mySubtitleList[0].toString(), myImageList[0], myReportInPDF[1], myReportInHTML[1], true)

        val informe2 = Reports(myImageNameList[1].toString(), mySubtitleList[1].toString(), myImageList[1], myReportInPDF[0], myReportInHTML[0], false)

        val informe3 = Reports(myImageNameList[2].toString(), mySubtitleList[2].toString(), myImageList[2], myReportInPDF[2], myReportInHTML[2], false)

        val informe4 = Reports(myImageNameList[3].toString(), mySubtitleList[3].toString(), myImageList[3], myReportInPDF[3], myReportInHTML[3], false)


        arraylistOfReports.add(informe)
        arraylistOfReports.add(informe2)
        arraylistOfReports.add(informe3)
        arraylistOfReports.add(informe4)

       val myAdapter = ReportsAdapter(this, arraylistOfReports) {

            val intent = Intent(this, ReportsDialog::class.java)
            intent.putExtra("report", it)
            startActivity(intent)

        }

        val columns = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 2 else 1
        recyclerReports.layoutManager = GridLayoutManager(applicationContext,columns)
        recyclerReports.adapter = myAdapter


    }

}
