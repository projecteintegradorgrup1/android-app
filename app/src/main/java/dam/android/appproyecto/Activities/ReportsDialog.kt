package dam.android.appproyecto.Activities

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import dam.android.appproyecto.R
import dam.android.appproyecto.model.Reports
import kotlinx.android.synthetic.main.activity_reports_dialog.*

class ReportsDialog : AppCompatActivity() {

    lateinit var report :Reports

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reports_dialog)

        report = intent.getSerializableExtra("report") as Reports
        this.title = this.title.toString()  +": "+ report.name

        if(!report.hasParameters){
            etParametro.visibility = View.GONE
            tvParametro.visibility = View.GONE
        }


    }


    fun onClick(v:View){
        when(v.id){
            R.id.btnHtml -> {
                if(report.hasParameters){
                    Toast.makeText(applicationContext,resources.getText(R.string.crearHTML), Toast.LENGTH_SHORT).show()
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(report.urlDirectionHTML+"&BuscaDNI="+etParametro.text))
                    startActivity(i)

                }else{
                    Toast.makeText(applicationContext, resources.getText(R.string.crearHTML), Toast.LENGTH_SHORT).show()
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(report.urlDirectionHTML))
                    startActivity(i)
                }
            }

            R.id.btnPDF -> {

                if(report.hasParameters){
                    Toast.makeText(applicationContext,resources.getText(R.string.crearPDF), Toast.LENGTH_SHORT).show()
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(report.urlDirectionPDF+"&BuscaDNI="+etParametro.text))
                    startActivity(i)
                }else {
                    Toast.makeText(applicationContext,resources.getText(R.string.crearPDF), Toast.LENGTH_SHORT).show()
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(report.urlDirectionPDF))
                    startActivity(i)

                }
            }
            R.id.btnCancelarReports -> {
                Toast.makeText(applicationContext,resources.getText(R.string.cancelando_la_operacion), Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

}
