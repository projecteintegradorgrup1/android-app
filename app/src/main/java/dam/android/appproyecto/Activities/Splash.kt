package dam.android.appproyecto.Activities

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import dam.android.appproyecto.utils.Constants
import dam.android.appproyecto.Controlador
import dam.android.appproyecto.R
import dam.android.appproyecto.model.User
import dam.android.appproyecto.utils.NetworkStateReceiver

class Splash : AppCompatActivity() {

    private val networkStateReceiver = NetworkStateReceiver()

    public override fun onResume() {
        super.onResume()
        registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

    }

    public override fun onPause() {
        unregisterReceiver(networkStateReceiver)
        super.onPause()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)



        // si hay un usuario en preferences cargar la boton activity sino el login
        comrpobarUsuarioLogeado()
        finish()
    }

    private fun comrpobarUsuarioLogeado() {
        val myPreferences = getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE)
        val name = (myPreferences.getString(Constants.Preferences.LOGIN_USER, ""))
        val asistente = (myPreferences.getString(Constants.Preferences.LOGIN_ASISTENTE, ""))

        if (name != "") {
            val id = myPreferences.getInt(Constants.Preferences.LOGIN_ID, -1)
            val ap1 = (myPreferences.getString(Constants.Preferences.LOGIN_APELLIDO1, ""))
            val ap2 = (myPreferences.getString(Constants.Preferences.LOGIN_APELLIDO2, ""))
            val email = (myPreferences.getString(Constants.Preferences.LOGIN_EMAIL, ""))
            val nss = (myPreferences.getString(Constants.Preferences.LOGIN_NSS, ""))
            val telefono = (myPreferences.getString(Constants.Preferences.LOGIN_TELEFONO, ""))
            val dni = (myPreferences.getString(Constants.Preferences.LOGIN_DNI, ""))

            val user = User(id, null, null, nss, name, ap1, ap2, telefono, email, dni)
            val intent = Intent(this, BotonActivity::class.java)
            intent.putExtra(Constants.KEY_LOGIN_USER, user)

            startActivity(intent)
        }else if(asistente!=""){
            startActivity(Intent(this,ReportsAsistente::class.java))

        }else{
            startActivity(Intent(this, Login::class.java))
        }
    }




}
