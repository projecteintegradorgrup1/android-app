package dam.android.appproyecto.Activities


import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_boton.*
import kotlinx.android.synthetic.main.app_bar_boton.*
import android.os.*
import android.preference.PreferenceManager
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import dam.android.appproyecto.utils.Constants
import dam.android.appproyecto.Controlador
import dam.android.appproyecto.R
import kotlinx.android.synthetic.main.content_boton.*
import dam.android.appproyecto.model.User
import dam.android.appproyecto.utils.NetworkStateReceiver
import dam.android.appproyecto.utils.NotificationUtil
import dam.android.appproyecto.utils.ServiciosUtil
import kotlinx.android.synthetic.main.content_notificaciones.*
import java.lang.ref.WeakReference
import java.sql.SQLException


class BotonActivity() : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    private var networkStateReceiver: NetworkStateReceiver = NetworkStateReceiver()
    private lateinit var alertLlamando:AlertDialog.Builder



    override fun onCreate(savedInstanceState: Bundle?) {


        // si es tablet la queremos en landscape siempre, sino siempre en portrait
        if(resources.getBoolean(R.bool.isTablet)){
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }else{
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boton)


        // para comprobar en las pruebas instrumentales
        button.contentDescription = "INSTRUMENTAL_TEST"


        setSupportActionBar(toolbar)

        // empezar servicio
        service()

        // preguntar permisos de localizacion
        permisos()

        // poner si no se han puesto y coger los valores de preferencias de ajustes
        setValuesSettings()

        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())

        alertLlamando = AlertDialog.Builder(this)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout_boton, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout_boton.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)


    }

    //region Permisos
    // preguntar permisos de localizacion si no lo tiene
    private fun permisos() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MapsActivity.LOCATION_PERMISSION_REQUEST_CODE)
        }

    }
    //endregion
    //region Service

    // si es la primera vez lanzar el service con la latencia cogida de preferences y lanzar una async task para que coga
    // las notificaciones la primera vez
    private fun service() {


        if (isFirstTimeService()) {
            // lanzar la async task si es la primera vez
            NotificacionesAsyncTask(this).execute()

            // lanzar  los jobs
            ServiciosUtil.scheduleJobNotificacion(this)
            ServiciosUtil.scheduleJobLocation(this)
            // poner a false el booleano first time
            getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).edit().putBoolean(Constants.Preferences.FIRST_TIME,false).apply()

        }

    }

    private fun isFirstTimeService(): Boolean {
        return getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).getBoolean(Constants.Preferences.FIRST_TIME,true)
    }






    class NotificacionesAsyncTask constructor(context: Context): AsyncTask<Void, Void, Void>() {

        var contextWeakReference: WeakReference<Context> = WeakReference(context)


        @TargetApi(Build.VERSION_CODES.O)
        @RequiresApi(Build.VERSION_CODES.O)
        override fun doInBackground(vararg params: Void?): Void? {
                val myPreferences = (contextWeakReference.get()!!.getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE))

                val id = myPreferences.getInt(Constants.Preferences.LOGIN_ID, -1)
                try {

                    if (Controlador.isConectividad()) {
                        val notificaciones = Controlador().setNotificacionesToSqlite(id, contextWeakReference.get())
                        if(notificaciones) {
                            val notificationUtil = NotificationUtil(contextWeakReference.get())
                            val nb = notificationUtil.getChannelNotification("", "")
                            notificationUtil.manager.notify(101, nb.build())
                        }
                    }


                } catch (e: SQLException) {
                    Log.e("SQLERROR", e.message)
                } catch (e: ClassNotFoundException) {
                    Log.e("SQLERROR", e.message)
                }

            return null
        }

    }
    //endregion
    //region Settings
    // si no hay valores de ajustes en preferencias, pone los por defecto, luego coge uno a uno
    private fun setValuesSettings() {
        // pone en el shared preferences los valores por defecto de los campos del archivo preferences.xml
        // el booleano si esta en false hace que se pondrán los valores solo si no se han puesto nunca
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false)
        PreferenceManager.setDefaultValues(this, R.xml.pref_data_sync, false)

        // cooger los valores, aunque sean los por defectos
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        val nightMode = sharedPref.getBoolean(Constants.Ajustes.NIGHT_MODE, false)
        if(nightMode){
           AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }
    //endregion
    //region User
    /**
     * coger los datos del intent y setearlo en la vista
     */
    private fun initUser() {
        if(intent.extras != null) {
            // coger valores del intent
            val user: User = intent.extras!!.getSerializable(Constants.KEY_LOGIN_USER) as User

            // poner valores en el header del nav drawer
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_name).text = user.nombre
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_ap1).text = user.apellido1
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_ap2).text = user.apellido2
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_nss).text = user.nss
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_dni).text = user.dni
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_telefono).text = user.telefono
        }else{
            val myPreferences = getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE)
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_name).text = myPreferences.getString(Constants.Preferences.LOGIN_USER,"")
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_ap1).text = myPreferences.getString(Constants.Preferences.LOGIN_APELLIDO1,"")
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_ap2).text = myPreferences.getString(Constants.Preferences.LOGIN_APELLIDO2,"")
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_nss).text = myPreferences.getString(Constants.Preferences.LOGIN_NSS,"")
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_dni).text = myPreferences.getString(Constants.Preferences.LOGIN_DNI,"")
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_telefono).text = myPreferences.getString(Constants.Preferences.LOGIN_TELEFONO,"")

        }
    }
    //endregion
    //region  Metodos Sobreescritos
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.boton, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {

            R.id.action_settings -> {
                val intent = Intent(this, Settings::class.java)
                startActivity(intent)
                finish()
                return true
            }
            R.id.action_notification -> {
                startActivity(Intent(this, Notificaciones::class.java))
                return true
            }
            R.id.action_location ->{
                startActivity(Intent(this, MapsActivity::class.java))
                return true
            }
            R.id.action_medicament -> {
                startActivity(Intent(this, Medicaments::class.java))
                return true
            }
            R.id.nav_vivienda -> {

                startActivityForResult(Intent(this, ViviendaActual::class.java), Constants.RESULT_CODE_VIVIENDA)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_notificaciones -> {
                startActivity(Intent(this, Notificaciones::class.java))
            }
            R.id.nav_location -> {
                startActivity(Intent(this, MapsActivity::class.java))
            }
            R.id.nav_medicaments ->{
                startActivity(Intent(this, Medicaments::class.java))
            }
            R.id.nav_setting -> {
                val intent = Intent(this, Settings::class.java)
                startActivity(intent)
                finish()
            }
            R.id.nav_vivienda -> {
                // devolverá la vivienda actual
                startActivityForResult(Intent(this, ViviendaActual::class.java), Constants.RESULT_CODE_VIVIENDA)
            }
        }

        drawer_layout_boton.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == Constants.RESULT_CODE_VIVIENDA){
            if(resultCode == Activity.RESULT_OK){

                val idDependiente = data!!.getIntExtra(Constants.RESULT_DEPENDIENTE,-1)
                val idVivienda = data.getIntExtra(Constants.RESULT_VIVIENDA,-1)
                val intVD = ArrayList<Int>()
                intVD.add(idVivienda)
                intVD.add(idDependiente)
                AsyncTaskSetViviendaActual(this).execute(intVD)

            }
        }
    }

    // async task que setea la vivienda actual
     class AsyncTaskSetViviendaActual constructor(context: Context): AsyncTask<ArrayList<Int>, Void, Void>() {

        private var contextWeakReference: WeakReference<Context> = WeakReference(context)

        override fun doInBackground(vararg params: ArrayList<Int>?): Void?{
            val intVD = params[0]
            Controlador().setViviendaActual(contextWeakReference.get(), intVD!![0], intVD[1])

            return null
        }

        override fun onPreExecute() {
            super.onPreExecute()
            (contextWeakReference.get() as BotonActivity).empezarAsyncTask()
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            (contextWeakReference.get() as BotonActivity).terminarAsyncTask()
        }
    }








    public override fun onResume() {
        initUser()
        super.onResume()
        registerReceiver(networkStateReceiver, IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION))

    }

    public override fun onPause() {
        unregisterReceiver(networkStateReceiver)
        super.onPause()
    }

    //endregion



    //region alarma
    fun empezarAsyncTask(){
        progressBarBoton.visibility = View.VISIBLE
    }
    fun terminarAsyncTask(){

        progressBarBoton.visibility = View.GONE
    }


    fun onClick(v :View){

        val maquina = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.Ajustes.IP,"0.0.0.0")
        val dni = getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).getString(Constants.Preferences.LOGIN_DNI,"-1")

        val MaquinaDni = arrayOf(maquina,dni)


        AlarmaTask(this).execute(MaquinaDni)
    }



     fun terminarLlamada(valid:Boolean){
        if(valid){

            // contestada
            val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
            alertDialog.setTitle(getString(R.string.titleEnLlamada))
            alertDialog.setMessage(getString(R.string.messageEnLlamada))

            alertDialog.setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
                dialog.cancel()
            }

            alertDialog.show()
        }else{

            // error
            val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
            alertDialog.setTitle(getString(R.string.titleLlamadaRechazada))
            alertDialog.setMessage(getString(R.string.messageLlamadaRechazada))

            alertDialog.setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
                dialog.cancel()
            }

            alertDialog.show()
        }
    }

    class AlarmaTask constructor(context:Context): AsyncTask<Array<String>, Void, Boolean>() {

        private val weakReferenceContext: WeakReference<Context> = WeakReference(context)

        override fun doInBackground(vararg params: Array<String>): Boolean {
            val maquina = params[0][0]
            val dni = params[0][1]
            val valid = Controlador().aviso(maquina,dni)
            return valid
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
            (weakReferenceContext.get() as BotonActivity).terminarLlamada(result)
        }
    }
//endregion


}
