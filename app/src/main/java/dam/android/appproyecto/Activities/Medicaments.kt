package dam.android.appproyecto.Activities

import android.app.AlertDialog
import android.content.Context
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import android.view.ContextThemeWrapper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import dam.android.appproyecto.*
import dam.android.appproyecto.model.Aviso
import dam.android.appproyecto.Adapters.AvisoAdapter
import dam.android.appproyecto.Controlador
import dam.android.appproyecto.utils.Constants
import dam.android.appproyecto.utils.NetworkStateReceiver

import kotlinx.android.synthetic.main.activity_medicaments.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.card_item.view.*
import kotlinx.android.synthetic.main.content_medicaments.*
import kotlinx.android.synthetic.main.content_notificaciones.*
import java.lang.ref.WeakReference

class Medicaments : AppCompatActivity() {

    private var networkStateReceiver: NetworkStateReceiver = NetworkStateReceiver()
    private var medicamentos:ArrayList<Aviso> = ArrayList()
    private lateinit var  myAdapter:AvisoAdapter
    // para comprobar si esta en la ventana de por hacer, asi cuando pulses uno se borrara o no
    private var isInPorHacer = true

    override fun onCreate(savedInstanceState: Bundle?) {

        val isTablet = resources.getBoolean(R.bool.isTablet)
        if (isTablet) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_medicaments)
        setSupportActionBar(toolbarMedicaments)
        setUI()


    }
    public override fun onResume() {
        super.onResume()
        registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

    }

    public override fun onPause() {
        unregisterReceiver(networkStateReceiver)
        super.onPause()
    }

    private fun setUI() {


        medicamentos.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.MEDICAMENT,false))
        val columns = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 2 else 1
        rvMedicaments.layoutManager = GridLayoutManager(this,columns)

        // a es el aviso y v es el view
        // le pasamos al async task el view por el constructor para que haga una weak reference
         myAdapter = AvisoAdapter(medicamentos,this){ a, v ->
            empezarTask()
            AsynckSetVisto(v, this).execute(a)
        }
        rvMedicaments.adapter = myAdapter

        comprobarVacio()

    }

    private fun comprobarVacio() {
        if(((rvMedicaments.adapter) as AvisoAdapter).isEmpty()){
            rvMedicaments.visibility = View.GONE
            emptyMedicaments.visibility = View.VISIBLE
        }else{
            rvMedicaments.visibility = View.VISIBLE
            emptyMedicaments.visibility = View.GONE
        }
    }

    //region AsyncTask
    fun empezarTask(){
        progressBar2.visibility = View.VISIBLE
        idParent.isEnabled = false
    }
    fun terminarTask(medicamentoVisto: Aviso){
        progressBar2.visibility = View.INVISIBLE
        idParent.isEnabled = true
        // si estaba en por hacer borramos del adapter el medicamento que ha pulsado
        if(isInPorHacer){
            val itemRemoved = medicamentos.indexOf(medicamentoVisto)
            medicamentos.remove(medicamentoVisto)
            myAdapter.notifyItemRemoved(itemRemoved)
        }
        comprobarVacio()
    }

    /**
     * Cuando inicia la async task pone visible el progress bar y hace el proceso de poner a visto la notificacion
     * cuando termina pone a invisible la progress bar
     */
     class AsynckSetVisto constructor(cardView: View, context: Context): AsyncTask<Aviso, Void, Aviso>() {

        private var cardWeakReference: WeakReference<View> = WeakReference(cardView)
        private var contextWeakReference: WeakReference<Context> = WeakReference(context)


        override fun doInBackground(vararg params: Aviso): Aviso {

            Controlador().setVistoNotificacion(params[0], contextWeakReference.get())
            return params[0]
        }

        override fun onPostExecute(result: Aviso) {
            super.onPostExecute(result)
            cardWeakReference.get()!!.card.changeMode(Constants.CARD_TYPES.NOTIFICATION_DONE)
            (contextWeakReference.get() as Medicaments).terminarTask(result)


        }

    }
    //endregion
    //region opciones
    // no queremos que salga el menu flotante cuando estamos en tablet

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val isTablet  = resources.getBoolean(R.bool.isTablet)
        if(!isTablet) {
            menuInflater.inflate(R.menu.medicamentos, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        isInPorHacer = false
        when(item.itemId){
            R.id.action_medicament_visto ->{
                optionSelectedVistos()
                return true
            }
            R.id.action_medicament_por_hacer ->{
                optionSelectedPorHacer()
                return true
            }

            R.id.action_medicament_todos ->{
                optionSelectedTodos()
                return true
            }


            else -> return super.onOptionsItemSelected(item)
        }

    }

    // on Tablet
    fun onClickTablet(v:View){
        isInPorHacer = false
        when(v.id){
            R.id.linearVistos ->{
                optionSelectedVistos()
                tvPorHacer.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvVistos.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
                tvTodos.setTextColor(ContextCompat.getColor(this,R.color.text))
            }
            R.id.linearPorHacer ->{
                optionSelectedPorHacer()
                tvPorHacer.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
                tvVistos.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvTodos.setTextColor(ContextCompat.getColor(this,R.color.text))
            }

            R.id.linearTodos ->{
                optionSelectedTodos()
                tvPorHacer.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvVistos.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvTodos.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
            }

        }
    }


    private fun optionSelectedVistos() {
        medicamentos.clear()
        medicamentos.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.MEDICAMENT,true))
        myAdapter.notifyDataSetChanged()
        comprobarVacio()
    }

    fun optionSelectedTodos(){
        medicamentos.clear()
        medicamentos.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.MEDICAMENT))
        myAdapter.notifyDataSetChanged()
        comprobarVacio()
    }
    fun optionSelectedPorHacer(){
        isInPorHacer = true
        medicamentos.clear()
        medicamentos.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.MEDICAMENT,false))
        myAdapter.notifyDataSetChanged()
        comprobarVacio()
    }
    //endregion

    //region alarma
    fun empezarAsyncTask(){
        progressBar2.visibility = View.VISIBLE
    }
    fun terminarAsyncTask(){
        progressBar2.visibility = View.GONE
    }


    fun onClickFab(v :View){
        empezarAsyncTask()
        val maquina = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.Ajustes.IP,"0.0.0.0")
        val dni = getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).getString(Constants.Preferences.LOGIN_DNI,"-1")

        val MaquinaDni = arrayOf(maquina,dni)


        AlarmaTask(this).execute(MaquinaDni)
    }



    fun terminarLlamada(valid:Boolean){
        if(valid){

            // contestada
            val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
            alertDialog.setTitle(getString(R.string.titleEnLlamada))
            alertDialog.setMessage(getString(R.string.messageEnLlamada))

            alertDialog.setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
                dialog.cancel()
            }

            alertDialog.show()
        }else{

            // error
            val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
            alertDialog.setTitle(getString(R.string.titleLlamadaRechazada))
            alertDialog.setMessage(getString(R.string.messageLlamadaRechazada))

            alertDialog.setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
                dialog.cancel()
            }

            alertDialog.show()
        }
        terminarAsyncTask()
    }

    class AlarmaTask constructor(context:Context): AsyncTask<Array<String>, Void, Boolean>() {

        private val weakReferenceContext: WeakReference<Context> = WeakReference(context)



        override fun doInBackground(vararg params: Array<String>): Boolean {
            val maquina = params[0][0]
            val dni = params[0][1]
            val valid = Controlador().aviso(maquina,dni)
            return valid
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
            (weakReferenceContext.get() as Medicaments).terminarLlamada(result)
        }
    }
//endregion
}
