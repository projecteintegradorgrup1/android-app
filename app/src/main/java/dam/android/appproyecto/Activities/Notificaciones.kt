package dam.android.appproyecto.Activities

import android.app.AlertDialog
import android.content.Context
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import android.view.ContextThemeWrapper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import dam.android.appproyecto.model.Aviso
import dam.android.appproyecto.Adapters.AvisoAdapter
import dam.android.appproyecto.utils.Constants
import dam.android.appproyecto.utils.NetworkStateReceiver
import dam.android.appproyecto.R
import dam.android.appproyecto.Controlador
import kotlinx.android.synthetic.main.activity_boton.*
import kotlinx.android.synthetic.main.activity_medicaments.*

import kotlinx.android.synthetic.main.activity_notificaciones.*
import kotlinx.android.synthetic.main.card_item.view.*
import kotlinx.android.synthetic.main.content_boton.*
import kotlinx.android.synthetic.main.content_medicaments.*
import kotlinx.android.synthetic.main.content_notificaciones.*
import java.lang.ref.WeakReference

class Notificaciones : AppCompatActivity() {



    private var networkStateReceiver: NetworkStateReceiver = NetworkStateReceiver()
    private var notificaciones:ArrayList<Aviso> = ArrayList()
    private lateinit var  myAdapter:AvisoAdapter
    // para comprobar si esta en la ventana de por hacer, asi cuando pulses uno se borrara o no, empieza en true porque la
    // principal es la de "por hacer"
    private var isInPorHacer = true

    override fun onPostCreate(savedInstanceState: Bundle?) {
        val isTablet = resources.getBoolean(R.bool.isTablet)
        if (isTablet) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
        super.onPostCreate(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notificaciones)
        setSupportActionBar(toolbar)
        setUI()


    }

    private fun setUI() {


        progressBarNotification.visibility = View.GONE
        notificaciones.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.NOTIFICATION,false))

        val columns = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 2 else 1
        rv_notifications.layoutManager = GridLayoutManager(this,columns)



        // a es el aviso y v es el view
        // le pasamos al async task el view por el constructor para que haga una weak reference
        myAdapter = AvisoAdapter(notificaciones,this){ a,v ->
            empezarTask()
            AsynckSetVisto(v, this).execute(a)
        }

        rv_notifications.adapter = myAdapter
        comprobarVacio()


    }

    private fun comprobarVacio() {

        if(((rv_notifications.adapter) as AvisoAdapter).isEmpty()){
            rv_notifications.visibility = View.GONE
            emptyNotificaciones.visibility = View.VISIBLE
        }else{
            rv_notifications.visibility = View.VISIBLE
            emptyNotificaciones.visibility = View.GONE
        }
    }
    public override fun onResume() {
        super.onResume()
        registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

    }

    public override fun onPause() {
        unregisterReceiver(networkStateReceiver)
        super.onPause()
    }

    //region AsyncTask
    /**
     * Cuando inicia la async task pone visible el progress bar y hace el proceso de poner a visto la notificacion
     * cuando termina pone a invisible la progress bar
     */
    fun empezarTask(){
        progressBarNotification.visibility = View.VISIBLE
        idParentNotificaciones.isEnabled = false
    }
    fun terminarTask(notificacion: Aviso){
        progressBarNotification.visibility = View.INVISIBLE
        idParentNotificaciones.isEnabled = true
        // si estaba en por hacer borramos del adapter el medicamento que ha pulsado
        if(isInPorHacer){
            val itemRemoved = notificaciones.indexOf(notificacion)
            notificaciones.remove(notificacion)
            myAdapter.notifyItemRemoved(itemRemoved)
        }
        comprobarVacio()

    }

    /**
     * Cuando inicia la async task pone visible el progress bar y hace el proceso de poner a visto la notificacion
     * cuando termina pone a invisible la progress bar
     */
    class AsynckSetVisto constructor(cardView: View, context: Context): AsyncTask<Aviso, Void, Aviso>() {

        private var cardWeakReference: WeakReference<View> = WeakReference(cardView)
        private var contextWeakReference: WeakReference<Context> = WeakReference(context)


        override fun doInBackground(vararg params: Aviso): Aviso {

            Controlador().setVistoNotificacion(params[0], contextWeakReference.get())

            return params[0]
        }

        override fun onPostExecute(result: Aviso) {
            super.onPostExecute(result)
            cardWeakReference.get()!!.card.changeMode(Constants.CARD_TYPES.NOTIFICATION_DONE)
            (contextWeakReference.get() as Notificaciones).terminarTask(result)


        }

    }
    //endregion

    //region opciones selected
    fun onClickTablet(v:View){
        isInPorHacer = false
        when(v.id){
            R.id.linearVistosN ->{
                optionSelectedVistos()
                tvPorHacerN.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvVistosN.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
                tvTodosN.setTextColor(ContextCompat.getColor(this,R.color.text))
            }
            R.id.linearPorHacerN ->{
                optionSelectedPorHacer()
                tvPorHacerN.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
                tvVistosN.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvTodosN.setTextColor(ContextCompat.getColor(this,R.color.text))
            }

            R.id.linearTodosN ->{
                optionSelectedTodos()
                tvPorHacerN.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvVistosN.setTextColor(ContextCompat.getColor(this,R.color.text))
                tvTodosN.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
            }

        }
    }
    // si es tablet no queremos mostrar el popup menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val isTablet  = resources.getBoolean(R.bool.isTablet)
        if(!isTablet) {
            menuInflater.inflate(R.menu.notificaciones, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        isInPorHacer = false
        when(item.itemId){
            R.id.action_notificacion_visto ->{
                optionSelectedVistos()
                return true
            }
            R.id.action_notificacion_por_hacer ->{
                optionSelectedPorHacer()
                return true
            }

            R.id.action_notificacion_todos ->{
                optionSelectedTodos()
                return true
            }


            else -> return super.onOptionsItemSelected(item)
        }

    }

    private fun optionSelectedVistos() {
        notificaciones.clear()
        notificaciones.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.NOTIFICATION,true))
        myAdapter.notifyDataSetChanged()
        comprobarVacio()
    }

    fun optionSelectedTodos(){
        notificaciones.clear()
        notificaciones.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.NOTIFICATION))
        myAdapter.notifyDataSetChanged()
        comprobarVacio()
    }
    fun optionSelectedPorHacer(){
        isInPorHacer = true
        notificaciones.clear()
        notificaciones.addAll(Controlador().getAvisos(this, Constants.CARD_TYPES.NOTIFICATION,false))
        myAdapter.notifyDataSetChanged()
        comprobarVacio()
    }
    //endregion


    //region alarma
    fun empezarAsyncTask(){
        progressBarNotification.visibility = View.VISIBLE
    }
    fun terminarAsyncTask(){
        progressBarNotification.visibility = View.GONE
    }


    fun onClickFab(v :View){
        empezarAsyncTask()
        val maquina = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.Ajustes.IP,"0.0.0.0")
        val dni = getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).getString(Constants.Preferences.LOGIN_DNI,"-1")

        val MaquinaDni = arrayOf(maquina,dni)


        AlarmaTask(this).execute(MaquinaDni)
    }



    fun terminarLlamada(valid:Boolean){
        if(valid){

            // contestada
            val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
            alertDialog.setTitle(getString(R.string.titleEnLlamada))
            alertDialog.setMessage(getString(R.string.messageEnLlamada))

            alertDialog.setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
                dialog.cancel()
            }

            alertDialog.show()
        }else{

            // error
            val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
            alertDialog.setTitle(getString(R.string.titleLlamadaRechazada))
            alertDialog.setMessage(getString(R.string.messageLlamadaRechazada))

            alertDialog.setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
                dialog.cancel()
            }

            alertDialog.show()
        }
        terminarAsyncTask()
    }

    class AlarmaTask constructor(context:Context): AsyncTask<Array<String>, Void, Boolean>() {

        private val weakReferenceContext: WeakReference<Context> = WeakReference(context)



        override fun doInBackground(vararg params: Array<String>): Boolean {
            val maquina = params[0][0]
            val dni = params[0][1]
            val valid = Controlador().aviso(maquina,dni)
            return valid
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
            (weakReferenceContext.get() as Notificaciones).terminarLlamada(result)
        }
    }
    //endregion

}
