package dam.android.appproyecto.Activities

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import dam.android.appproyecto.model.Aviso
import dam.android.appproyecto.utils.Constants
import dam.android.appproyecto.R
import kotlinx.android.synthetic.main.my_card_view.view.*


class CustomCardView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : LinearLayout (context, attrs,defStyle,defStyleRes){



    init {
        LayoutInflater.from(context)
                .inflate(R.layout.my_card_view, this, true)
        orientation = VERTICAL



       attrs?.let {
           val typedArray = context.obtainStyledAttributes(it, R.styleable.card_component_attributes,0,0)
           val title  = typedArray.getString(R.styleable.card_component_attributes_title)
           val subtitle = typedArray.getString(R.styleable.card_component_attributes_subtitle)
           val hora = typedArray.getString(R.styleable.card_component_attributes_hora)
           val mode  = typedArray.getString(R.styleable.card_component_attributes_mode)

            // set attributes to the view
            tvTitle.text = title
            tvHora.text = hora
            tvSubtitle.text = subtitle
            // depende del atributo mode cambiamos el color de la card, por defecto será NOTIFICATION
            when(mode){
                Constants.CARD_TYPES.NOTIFICATION ->{
                    vColor.background = resources.getDrawable(R.drawable.shape_card_gray,null)
                    ivState.setImageResource(R.drawable.tickgray)
                }
                Constants.CARD_TYPES.NOTIFICATION_DONE ->{
                    vColor.background = resources.getDrawable(R.drawable.shape_card_green,null)
                    ivState.setImageResource(R.drawable.tick)
                }
                Constants.CARD_TYPES.MEDICAMENT ->{
                    vColor.background = resources.getDrawable(R.drawable.shape_card_medicament,null)
                    ivState.setImageResource(R.drawable.plus)
                }
                else ->{
                    vColor.background = resources.getDrawable(R.drawable.shape_card_gray,null)
                    ivState.setImageResource(R.drawable.tickgray)
                }
            }
            typedArray.recycle()

        }


    }



    fun setValues(aviso: Aviso){
        tvTitle.text = aviso.titulo
        tvSubtitle.text = aviso.descripcion
        tvHora.text = aviso.fecha
        when(aviso.modo){
            Constants.CARD_TYPES.NOTIFICATION ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_gray,null)
                ivState.setImageResource(R.drawable.tareas)
            }
            Constants.CARD_TYPES.NOTIFICATION_DONE ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_green,null)
                ivState.setImageResource(R.drawable.tick)
            }
            Constants.CARD_TYPES.MEDICAMENT ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_medicament,null)
                ivState.setImageResource(R.drawable.medicament)
            }
            else ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_gray,null)
                ivState.setImageResource(R.drawable.tickgray)
            }
        }

        invalidate()
    }


    fun changeMode(mode:String){
        when(mode){
            Constants.CARD_TYPES.NOTIFICATION ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_gray,null)
                ivState.setImageResource(R.drawable.tickgray)
            }
            Constants.CARD_TYPES.NOTIFICATION_DONE ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_green,null)
                ivState.setImageResource(R.drawable.tick)
            }
            Constants.CARD_TYPES.MEDICAMENT ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_medicament,null)
                ivState.setImageResource(R.drawable.plus)
            }
            else ->{
                vColor.background = resources.getDrawable(R.drawable.shape_card_gray,null)
                ivState.setImageResource(R.drawable.tickgray)
            }
        }
        invalidate()
    }

}