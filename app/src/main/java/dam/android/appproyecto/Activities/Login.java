package dam.android.appproyecto.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.sql.SQLException;

import dam.android.appproyecto.model.Reports;
import dam.android.appproyecto.utils.Constants;
import dam.android.appproyecto.Controlador;
import dam.android.appproyecto.R;
import dam.android.appproyecto.model.User;
import dam.android.appproyecto.utils.NetworkStateReceiver;

public class Login extends AppCompatActivity {

    private NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();

    public void onResume() {
        super.onResume();
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

    }

    public void onPause() {
        unregisterReceiver(networkStateReceiver);
        super.onPause();
    }

    private EditText etDni;
    private EditText etContra;
    private ProgressBar progressBar;
    private Button btnLogin;

    /*
        El login funciona siguiendo estos pasos:
        Antes de hacer el sent content view comprobamos en el preferences si tine registrado un login. Puede iniciarse en offline ( se guardará en sqlite)
        se intentará cada x tiempo actualizar la bd si hay conexion ( Async Task o service )

        Cuando se pulsa el boton de login se cogen los valores de los edit texts
        se llama al metodo empezarAsyncTask que bloquea la ui y pone visible el progres bar
        se inicia el asynctask pasandole los valores, cuando termina llama al metodo validar, que llama al metodo terminar async task,
        el metodo validar recibe un booleano de la async task que será true si existe un usuario con los credenciales indicados
        si es true se iniciará la siguiente acitvity
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        if(isTablet){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }



        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btnLogin);

        etContra = findViewById(R.id.etContra);
        etDni = findViewById(R.id.etDni);
        progressBar = findViewById(R.id.progressBar);

        btnLogin.setOnClickListener(v -> {

            String[] credenciales = new String[2];
            credenciales[0] = etDni.getText().toString();
            credenciales[1] = etContra.getText().toString();
            AsyncLogin login = new AsyncLogin(this);
            empezarAsynTask();
            login.execute(credenciales);

        });



    }


    // metodo que se llama cuando se termina la asynctask de login
    // comprueba si no ha dado error, guarda los credenciales en preferences y lanza la boton activity o la de reports
    public void validar(User user){
        terminarAsynTask();
        if(!user.error()){

            if(user.isAsistente()){
                // guardar en preferences para cuando ejecute posteriormente y lanze directo la activity reports
                SharedPreferences myPreferences = getSharedPreferences(Constants.Preferences.FILE_NAME,MODE_PRIVATE);
                SharedPreferences.Editor editor = myPreferences.edit();
                editor.putString(Constants.Preferences.LOGIN_ASISTENTE,String.valueOf(user.isAsistente()));
                editor.apply();

                Intent reportAcitvity = new Intent(this,ReportsAsistente.class);
                startActivity(reportAcitvity);

            }else{
                guardarEnPreferences(user);
                Intent botonIntent = new Intent(this,BotonActivity.class);

                // datos a mostrar en el nav drawer
                botonIntent.putExtra(Constants.KEY_LOGIN_USER,user);
                startActivity(botonIntent);
            }

            finish();

        }else{
            loginError();
        }
    }


    private void empezarAsynTask(){
        progressBar.setVisibility(View.VISIBLE);
        etContra.setClickable(false);
        etDni.setClickable(false);
        btnLogin.setClickable(false);

    }
    private void terminarAsynTask(){
        progressBar.setVisibility(View.GONE);
        etContra.setClickable(true);
        etDni.setClickable(true);
        btnLogin.setClickable(true);
    }


    private void loginError() {
        terminarAsynTask();
        btnLogin.setContentDescription("INSTRUMENTAL_TEST");
        Toast.makeText(this,getString(R.string.login_error),Toast.LENGTH_LONG).show();
    }

    private void guardarEnPreferences(User user) {

        SharedPreferences myPreferences = getSharedPreferences(Constants.Preferences.FILE_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreferences.edit();

        // poner los credenciales
        editor.putString(Constants.Preferences.LOGIN_DNI,etDni.getText().toString());
        editor.putString(Constants.Preferences.LOGIN_CONTRA,etContra.getText().toString());
        // datos
        editor.putInt(Constants.Preferences.LOGIN_ID,user.getId());
        editor.putString(Constants.Preferences.LOGIN_USER,user.getNombre());
        editor.putString(Constants.Preferences.LOGIN_APELLIDO1,user.getApellido1());
        editor.putString(Constants.Preferences.LOGIN_APELLIDO2,user.getApellido2());
        editor.putString(Constants.Preferences.LOGIN_NSS,user.getNss());
        editor.putString(Constants.Preferences.LOGIN_TELEFONO,user.getTelefono());
        editor.putString(Constants.Preferences.LOGIN_DNI,user.getDni());

        editor.commit();

    }


    // async task que logea y guarda las viviendas si no ha dado error
    public static class AsyncLogin extends AsyncTask<String[],Void,User>{

        WeakReference<Context> contextWeakReference;

        private AsyncLogin(Context context){
            contextWeakReference = new WeakReference<>(context);
        }

        @Override
        protected User doInBackground(String[]... strings) {
            User user = new User();
            try {

                String[] credentials = strings[0];
                String dni = credentials[0];
                String password = credentials[1];

                Controlador c = new Controlador();
                user = c.getLogin(dni,password,contextWeakReference.get());


            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                Log.i("conexion","not found class: " +e.getMessage());
                user.setError();
            } catch (SQLException e) {
                e.printStackTrace();
                Log.i("conexion","sql exception: "+e.getMessage());
                user.setError();
            }


            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            ((Login)contextWeakReference.get()).validar(user);
        }
    }


    //region ciclo de vida

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("dni",etDni.getText().toString());
        outState.putString("contra",etContra.getText().toString());


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        etDni.setText(savedInstanceState.getString("dni"));
        etContra.setText(savedInstanceState.getString("contra"));
    }

    //endregion


}
