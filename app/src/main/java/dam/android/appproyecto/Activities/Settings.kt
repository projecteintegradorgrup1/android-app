package dam.android.appproyecto.Activities

import android.app.AlertDialog
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_settings.*
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.view.ContextThemeWrapper
import android.widget.RadioButton
import android.widget.RadioGroup
import dam.android.appproyecto.utils.Constants
import dam.android.appproyecto.R
import dam.android.appproyecto.utils.NetworkStateReceiver


class Settings : AppCompatActivity() {


    private val networkStateReceiver = NetworkStateReceiver()

    public override fun onResume() {
        super.onResume()
        registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

    }

    public override fun onPause() {
        unregisterReceiver(networkStateReceiver)
        super.onPause()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,BotonActivity::class.java))

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // solo si es tablet
        val isTablet = resources.getBoolean(R.bool.isTablet)
        if (isTablet) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        titleDayNight.isClickable = false

        valoresPorDefecto()
    }

    private fun valoresPorDefecto() {
        val preferenceManager = PreferenceManager.getDefaultSharedPreferences(this)
        subtitleIpTeleoperador.text = preferenceManager.getString(Constants.Ajustes.IP,"")

        titleDayNight.isChecked = preferenceManager.getBoolean(Constants.Ajustes.NIGHT_MODE,false)

        subtitleLocation.text = preferenceManager.getString(Constants.Ajustes.SYNC_LOCATION, Constants.Ajustes.SYNC_LOCATION_DEFAULT)
        subtitleNotification.text = preferenceManager.getString(Constants.Ajustes.SYNC_NOTIFICATION, Constants.Ajustes.SYNC_NOTIFICATION_DEFAULT)

    }


    fun onClick(v: View){

        val id = v.id
        when (id){
            R.id.settTheme -> {
                cambiarTema()
            }
            R.id.settingIp -> {
                dialogInputTextIp()
            }
            R.id.freqNotification -> {
                showRadioButtonDialogNotification()
            }
            R.id.freqLocation -> {
                showRadioButtonDialogLocation()
            }
        }


    }
    //region tablet
    fun onClickTablet(v:View){
        when(v.id){
            R.id.linearGeneral -> {
                prefGeneralClicked()
            }
            R.id.linearData -> {
                prefDataClicked()
            }
        }
    }

    fun prefGeneralClicked(){
        // cambiar el color de los texts views
        tvGeneral.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
        tvData.setTextColor(ContextCompat.getColor(this,R.color.text))
        // mostrar los ajustes generales
        settTheme.visibility = View.VISIBLE
        settingIp.visibility = View.VISIBLE
        freqLocation.visibility = View.GONE
        freqNotification.visibility = View.GONE
    }
    fun prefDataClicked(){
        // cambiar el color de los texts views
        tvGeneral.setTextColor(ContextCompat.getColor(this,R.color.text))
        tvData.setTextColor(ContextCompat.getColor(this,R.color.colorAccent))
        // mostrar los ajustes de data y sync
        settTheme.visibility = View.GONE
        settingIp.visibility = View.GONE
        freqLocation.visibility = View.VISIBLE
        freqNotification.visibility = View.VISIBLE
    }
    //endregion
    private fun dialogInputTextIp() {

        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
        alertDialog.setTitle(getString(R.string.pref_title_display_ip))

        val  input =  EditText(this)
        val lp :LinearLayout.LayoutParams =  LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT)
        input.setLayoutParams(lp);
        alertDialog.setView(input)


        alertDialog.setPositiveButton(getString(R.string.accept)) { dialog, which ->
            val editor = PreferenceManager.getDefaultSharedPreferences(this).edit()
            editor.putString(Constants.Ajustes.IP,input.text.toString())
            editor.apply()
            subtitleIpTeleoperador.text = input.text
        }


        alertDialog.setNegativeButton(getString(R.string.cancelar)) { dialog, which -> dialog.cancel() }

        alertDialog.show()


    }

    private fun cambiarTema() {
        titleDayNight.isChecked = !titleDayNight.isChecked
        val preferenceManager = PreferenceManager.getDefaultSharedPreferences(this)
        val editor=  preferenceManager.edit().putBoolean(Constants.Ajustes.NIGHT_MODE,titleDayNight.isChecked)
        editor.apply()
        if(titleDayNight.isChecked){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            // si esta en modo noche ponemos contentdescription NOCHE
            titleDayNight.contentDescription = "NOCHE"
        }else{
            // si esta en modo noche ponemos contentdescription DIA
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            titleDayNight.contentDescription = "DIA"
        }
        recreate()
    }


    // coge todos los titles values de string, coger el actual de preferences
    private fun showRadioButtonDialogNotification() {

        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
        alertDialog.setTitle(getString(R.string.pref_header_notifications))

        val  radioGroup =  RadioGroup(this)
        val lp :LinearLayout.LayoutParams =  LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT)
        radioGroup.setLayoutParams(lp)

        val titlesValues = resources.getStringArray(R.array.pref_notification_sync_frequency_titles)
        val values = resources.getStringArray(R.array.pref_notification_sync_frequency_values)
        var cont = -1

        val actualNotification = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.Ajustes.SYNC_NOTIFICATION_VALUE, Constants.Ajustes.SYNC_NOTIFICATION_DEFAULT)


        for( v : String in titlesValues){
            val rb = RadioButton(applicationContext)
            rb.tag = cont // para luego coger el valor del array values y ponerlo en preferences
            rb.text = v
            rb.setTextColor(ContextCompat.getColor(this, R.color.text))
            rb.id = ++cont
            radioGroup.addView(rb)

            if(values[cont].equals(actualNotification)){
                radioGroup.check(rb.id)
            }
        }



        alertDialog.setView(radioGroup)


        alertDialog.setPositiveButton(getString(R.string.accept)) { dialog, which ->
            val editor = PreferenceManager.getDefaultSharedPreferences(this).edit()
            val arrayPosition = radioGroup.checkedRadioButtonId
            val value = values[arrayPosition]
            val title = titlesValues[arrayPosition]

            editor.putString(Constants.Ajustes.SYNC_NOTIFICATION_VALUE,value)
            editor.putString(Constants.Ajustes.SYNC_NOTIFICATION,title)

            subtitleNotification.text = title
            editor.apply()
        }


        alertDialog.setNegativeButton(getString(R.string.cancelar)) { dialog, which -> dialog.cancel() }

        alertDialog.show()

    }

    private fun showRadioButtonDialogLocation() {

        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this,R.style.myAlertDialog))
        alertDialog.setTitle(getString(R.string.pref_title_location_sync_frequency))

        val  radioGroup =  RadioGroup(this)
        val lp :LinearLayout.LayoutParams =  LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT)
        radioGroup.setLayoutParams(lp)

        val titlesValues = resources.getStringArray(R.array.pref_location_sync_frequency_titles)
        val values = resources.getStringArray(R.array.pref_location_sync_frequency_values)
        var cont = -1

        val actualNotification = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.Ajustes.SYNC_LOCATION_VALUE, Constants.Ajustes.SYNC_LOCATION_DEFAULT)


        for( v : String in titlesValues){
            val rb = RadioButton(applicationContext)
            rb.tag = cont // para luego coger el valor del array values y ponerlo en preferences
            rb.text = v
            rb.setTextColor(ContextCompat.getColor(this, R.color.text))
            rb.id = ++cont
            radioGroup.addView(rb)

            if(values[cont].equals(actualNotification)){
                radioGroup.check(rb.id)
            }
        }



        alertDialog.setView(radioGroup)


        alertDialog.setPositiveButton(getString(R.string.accept)) { dialog, which ->
            val editor = PreferenceManager.getDefaultSharedPreferences(this).edit()
            val arrayPosition = radioGroup.checkedRadioButtonId
            val value = values[arrayPosition]
            val title = titlesValues[arrayPosition]

            editor.putString(Constants.Ajustes.SYNC_LOCATION_VALUE,value)
            editor.putString(Constants.Ajustes.SYNC_LOCATION,title)

            subtitleLocation.text = title
            editor.apply()
        }


        alertDialog.setNegativeButton(getString(R.string.cancelar)) { dialog, which -> dialog.cancel() }

        alertDialog.show()

    }

}
