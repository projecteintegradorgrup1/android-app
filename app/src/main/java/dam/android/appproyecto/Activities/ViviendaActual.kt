package dam.android.appproyecto.Activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.activity_vivienda_actual.*
import android.widget.RadioButton
import dam.android.appproyecto.utils.Constants
import dam.android.appproyecto.Controlador
import dam.android.appproyecto.R
import dam.android.appproyecto.model.Vivienda
import dam.android.appproyecto.utils.NetworkStateReceiver
import java.lang.ref.WeakReference


class ViviendaActual : AppCompatActivity() {

    private var networkStateReceiver: NetworkStateReceiver = NetworkStateReceiver()
    private var id:Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {

        id = getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).getInt(Constants.Preferences.LOGIN_ID,-1)

        super.onCreate(savedInstanceState)
        // quitarle el titulo por defecto
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.activity_vivienda_actual)

        setUI()
    }
    public override fun onResume() {
        super.onResume()
        registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

    }

    public override fun onPause() {
        unregisterReceiver(networkStateReceiver)
        super.onPause()
    }

    private fun setUI() {
        progressBarVivienda.visibility = View.VISIBLE
       AsyncGetViviendas(id,this).execute()


    }


    private fun setViviendas(viviendas: ArrayList<Vivienda>) {


        // le pongo la id de la vivienda
        var cont = 0
        for(v: Vivienda in viviendas){
            val rb = RadioButton(applicationContext)
            // le pong la id de la vivienda en el tag para luego pasarsela al controlador en el listener
            rb.tag = v.id
            rb.text = v.toString()
            rb.setTextColor(ContextCompat.getColor(this, R.color.text))
            rb.id = cont++
            viviendasRadioGroup.addView(rb)

            if(v.actual){
                viviendasRadioGroup.check(rb.id)
            }

        }

        viviendasRadioGroup.setOnCheckedChangeListener { group, checkedId ->

            val resultIntent = Intent()

            resultIntent.putExtra(Constants.RESULT_VIVIENDA, findViewById<RadioButton>(checkedId).tag as Int)
            resultIntent.putExtra(Constants.RESULT_DEPENDIENTE, getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).getInt(Constants.Preferences.LOGIN_ID,-1))

            setResult(Activity.RESULT_OK, resultIntent)

        }
        progressBarVivienda.visibility = View.GONE
    }


    class AsyncGetViviendas(id: Int, context: Context) : AsyncTask<Void, Void, ArrayList<Vivienda>>() {


        private val idDependiente = id
        private val weakReferencecontexto = WeakReference(context)


        override fun doInBackground(vararg params: Void?): ArrayList<Vivienda> {

            return  Controlador().getViviendas(idDependiente,weakReferencecontexto.get())


        }

        override fun onPostExecute(viviendas: ArrayList<Vivienda>) {
            (weakReferencecontexto.get() as ViviendaActual).setViviendas(viviendas)
        }



    }




}

