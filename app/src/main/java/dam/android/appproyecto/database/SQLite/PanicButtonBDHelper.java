package dam.android.appproyecto.database.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PanicButtonBDHelper extends SQLiteOpenHelper {

    private static PanicButtonBDHelper instanceDBHelper;

    public static synchronized PanicButtonBDHelper getInstance(Context context){
        if(instanceDBHelper == null){
            instanceDBHelper = new PanicButtonBDHelper(context.getApplicationContext());
        }
        return instanceDBHelper;
    }


    private PanicButtonBDHelper(Context context){
        super(context,PanicButtonBDContract.DB_NAME,null,PanicButtonBDContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PanicButtonBDContract.Location.CREATE_TABLE);
        db.execSQL(PanicButtonBDContract.Avisos.CREATE_TABLE);
        db.execSQL(PanicButtonBDContract.Vivienda.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(PanicButtonBDContract.Location.DELETE_TABLE);
        db.execSQL(PanicButtonBDContract.Avisos.DELETE_TABLE);
        db.execSQL(PanicButtonBDContract.Vivienda.DELETE_TABLE);

        onCreate(db);
    }
}
