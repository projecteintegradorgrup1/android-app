package dam.android.appproyecto.database.SQLite;

public final class PanicButtonBDContract {

    public static final String DB_NAME = "PANICBUTTON.DB";
    public static final int DB_VERSION = 1;

    private PanicButtonBDContract(){

    }


    /**
     * cuando inicie la aplicación coger todas los avisos del dia y guardarlo en sqlite
     * EQUIVALENTES ENTRE BD -> SQLITE
     *
     * TABLA AVISO: --------
     * ID -> ID_AVISO
     * tipo -> TIPO
     * descripcion -> titulo
     *
     * TABLA AVISO_PERIOCIDAD: ---------
     * ID -> ID_AVISO_PERIOCIDAD
     * hora_fecha -> FECHA
     * descripcion -> descripcion
     * visto -> visto
     *
     * TUPLA DE TABLA SQLITE:
     * ( ID, ID_AVISO, ID_PERIOCIDAD, TIPO, TITULO, MODO , DESCRIPCION, FECHA, VISTO)
     *
     */
    public static class Avisos{
        public static final String TABLE_NAME = "AVISOS";


        public static final String _ID = "_id";
        public static final String ID_AVISO= "id_aviso";
        public static final String ID_AVISO_PERIOCIDAD= "id_aviso_periocidad";
        public static final String TITULO = "titulo";
        public static final String TIPO = "tipo";
        public static final String MODO = "modo";
        public static final String FECHA = "fecha";
        public static final String DESCRIPCION = "descripcion";
        public static final String VISTO = "visto";

        public static final String CREATE_TABLE = "CREATE TABLE "+Avisos.TABLE_NAME
                + " ("
                + Avisos._ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Avisos.ID_AVISO + " INTEGER NOT NULL, "
                + Avisos.ID_AVISO_PERIOCIDAD + " INTEGER NOT NULL, "
                + Avisos.TITULO +" TEXT NOT NULL, "
                + Avisos.TIPO + " TEXT NOT NULL, "
                + Avisos.MODO + " TEXT NOT NULL, "
                + Avisos.DESCRIPCION + " TEXT NOT NULL, "
                + Avisos.FECHA + " TEXT NOT NULL, "
                // sqlite no tiene boolean, guarda 0 ó 1
                + Avisos.VISTO + " INTEGER NOT NULL"
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS "+Avisos.TABLE_NAME;

    }


    // tabla que guardará la localización en caso de no haber conexión
    public static class Location{

        public static final String TABLE_NAME = "LOCATION";

        // id de la tabla de sqlite
        public static final String _ID = "_id";


        // coordenadas
        public static final String LATITUD= "latitud";
        public static final String LONGITUD = "longitud";

        // fecha
        public static final String FECHA = "fecha";

        public static final String CREATE_TABLE = "CREATE TABLE "+Location.TABLE_NAME
                + " ("
                + Location._ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Location.LATITUD+ " FLOAT NOT NULL, "
                + Location.LONGITUD + " FLOAT NOT NULL, "
                // sqlite no tiene campo date
                + Location.FECHA + " TEXT NOT NULL"
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS "+Location.TABLE_NAME;
    }


    public static class Vivienda{
        public static final String TABLE_NAME = "VIVIENDAS";
        public static final String _ID = "_id";
        public static final String ID_VIVIENDA = "id_vivienda";
        public static final String TIPO_VIA = "tipo_via";
        public static final String DIRECCION = "direccion";
        public static final String NUM = "num";
        public static final String ACTUAL = "actual";

        public static final String CREATE_TABLE = "CREATE TABLE "+Vivienda.TABLE_NAME
                + " ("
                + Vivienda._ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                +Vivienda.ID_VIVIENDA + " INTEGER NOT NULL, "
                + Vivienda.TIPO_VIA+ " TEXT NOT NULL, "
                + Vivienda.DIRECCION + " TEXT NOT NULL, "
                + Vivienda.NUM + " TEXT NOT NULL,"
                + Vivienda.ACTUAL + " INT NOT NULL"
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS "+Vivienda.TABLE_NAME;
    }



}
