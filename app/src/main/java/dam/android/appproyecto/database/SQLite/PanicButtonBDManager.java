package dam.android.appproyecto.database.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Timestamp;
import java.util.ArrayList;

import dam.android.appproyecto.model.Aviso;
import dam.android.appproyecto.utils.Constants;
import dam.android.appproyecto.model.Vivienda;

public class PanicButtonBDManager {



    private PanicButtonBDHelper panicButtonBDHelper;

    public  PanicButtonBDManager(Context context){
        panicButtonBDHelper = PanicButtonBDHelper.getInstance(context);
    }
    public void close(){
        panicButtonBDHelper.close();
    }

    //region Avisos
    // insert aviso
    public void insert(Aviso aviso){

        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();

        if(sqLiteDatabase !=null){
            ContentValues contentValues = new ContentValues();
            contentValues.put(PanicButtonBDContract.Avisos.ID_AVISO,aviso.getIdAviso());
            contentValues.put(PanicButtonBDContract.Avisos.ID_AVISO_PERIOCIDAD,aviso.getIdPeriocidad());
            contentValues.put(PanicButtonBDContract.Avisos.TITULO,aviso.getTitulo());
            contentValues.put(PanicButtonBDContract.Avisos.TIPO,aviso.getTipo());
            contentValues.put(PanicButtonBDContract.Avisos.MODO,aviso.getTipo());
            contentValues.put(PanicButtonBDContract.Avisos.DESCRIPCION,aviso.getDescripcion());
            contentValues.put(PanicButtonBDContract.Avisos.FECHA,aviso.getFecha());
            contentValues.put(PanicButtonBDContract.Avisos.VISTO,aviso.getVisto());

            sqLiteDatabase.insert(PanicButtonBDContract.Avisos.TABLE_NAME,null,contentValues);
        }


    }


    /**
     * SELECT AVISOS
     *  solo se llamará para pintar las card views, por lo que cogemos los datos
     *  necesarios para ello
     *
     */
    public Cursor getAvisos(String tipo){
        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getReadableDatabase();
        if(sqLiteDatabase!=null){
            String[] projection = new String[]{
                    PanicButtonBDContract.Avisos.ID_AVISO,
                    PanicButtonBDContract.Avisos.ID_AVISO_PERIOCIDAD,
                    PanicButtonBDContract.Avisos.TITULO,
                    PanicButtonBDContract.Avisos.TIPO,
                    PanicButtonBDContract.Avisos.MODO,
                    PanicButtonBDContract.Avisos.DESCRIPCION,
                    PanicButtonBDContract.Avisos.FECHA,
                    PanicButtonBDContract.Avisos.VISTO
            };

            cursor = sqLiteDatabase.query(PanicButtonBDContract.Avisos.TABLE_NAME,
                    projection,
                    PanicButtonBDContract.Avisos.TIPO + " = ?",
                    new String[]{tipo},
                    null,
                    null,
                    null);
        }
        return cursor;

    }

    /**
     * Select avisos con filtro
     * @param tipo medicamentos o notificaciones
     * @return avisos
     */
    public Cursor getAvisos(String tipo,boolean vistos){
        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getReadableDatabase();
        if(sqLiteDatabase!=null){
            String[] projection = new String[]{
                    PanicButtonBDContract.Avisos.ID_AVISO,
                    PanicButtonBDContract.Avisos.ID_AVISO_PERIOCIDAD,
                    PanicButtonBDContract.Avisos.TITULO,
                    PanicButtonBDContract.Avisos.TIPO,
                    PanicButtonBDContract.Avisos.MODO,
                    PanicButtonBDContract.Avisos.DESCRIPCION,
                    PanicButtonBDContract.Avisos.FECHA,
                    PanicButtonBDContract.Avisos.VISTO
            };
            String visto = vistos ? "1" :"0";
            cursor = sqLiteDatabase.query(PanicButtonBDContract.Avisos.TABLE_NAME,
                        projection,
                        PanicButtonBDContract.Avisos.TIPO + " = ? and " + PanicButtonBDContract.Avisos.VISTO + " = ?",
                        new String[]{tipo, visto},
                        null,
                        null,
                        null);

        }
        return cursor;

    }


    // devuelve todos los ids de la tabla aviso periodicidad los cuales tienen el modo DONE en sqlite
    public Cursor getAvisosVisto(){
        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getReadableDatabase();
        if(sqLiteDatabase!=null){
            String[] projection = new String[]{
                    PanicButtonBDContract.Avisos.ID_AVISO_PERIOCIDAD
            };

            cursor = sqLiteDatabase.query(PanicButtonBDContract.Avisos.TABLE_NAME,
                    projection,
                    PanicButtonBDContract.Avisos.MODO + " = ?",
                    new String[]{Constants.CARD_TYPES.NOTIFICATION_DONE},
                    null,
                    null,
                    null);
        }
        return cursor;

    }




    public void deletAvisos() {
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();
        if(sqLiteDatabase!=null){
            sqLiteDatabase.delete(PanicButtonBDContract.Avisos.TABLE_NAME,null,null);
        }
    }

    public void setVistoAviso(Aviso aviso) {
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();
        if(sqLiteDatabase!=null){
            ContentValues contentValues = new ContentValues();
            int visto = 1;// true para sqlite
            aviso.setVisto(true);
            contentValues.put(PanicButtonBDContract.Avisos.VISTO,visto);
            contentValues.put(PanicButtonBDContract.Avisos.MODO,Constants.CARD_TYPES.NOTIFICATION_DONE);
            sqLiteDatabase.update(PanicButtonBDContract.Avisos.TABLE_NAME,contentValues,PanicButtonBDContract.Avisos.ID_AVISO_PERIOCIDAD + "= ?",new String[]{String.valueOf(aviso.getIdPeriocidad())});
        }
    }
    //endregion
    //region VIVIENDAS
    public Cursor getViviendas() {

        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getReadableDatabase();

        if(sqLiteDatabase!=null){
            String[] projection = new String[]{
                    PanicButtonBDContract.Vivienda.ID_VIVIENDA,
                    PanicButtonBDContract.Vivienda.TIPO_VIA,
                    PanicButtonBDContract.Vivienda.DIRECCION,
                    PanicButtonBDContract.Vivienda.NUM,
                    PanicButtonBDContract.Vivienda.ACTUAL,
            };

            cursor = sqLiteDatabase.query(PanicButtonBDContract.Vivienda.TABLE_NAME,projection,null,null,null,null,null);

        }

        return cursor;

    }
    public void insertViviendas(ArrayList<Vivienda> viviendas) {
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();
        if(sqLiteDatabase!=null){
            ContentValues contentValues = new ContentValues();
            for(Vivienda v : viviendas){

                // 1 si true, el sqlite no acepta booleanos
                int actual = v.getActual() ? 1 : 0;

                contentValues.put(PanicButtonBDContract.Vivienda.ID_VIVIENDA,v.getId());
                contentValues.put(PanicButtonBDContract.Vivienda.TIPO_VIA,v.getTipoVia());
                contentValues.put(PanicButtonBDContract.Vivienda.DIRECCION,v.getDireccion());
                contentValues.put(PanicButtonBDContract.Vivienda.NUM,v.getNum());
                contentValues.put(PanicButtonBDContract.Vivienda.ACTUAL,actual);

                sqLiteDatabase.insert(PanicButtonBDContract.Vivienda.TABLE_NAME,null,contentValues);
            }
        }


    }

    // poner todas a false y luego a true la actual
    public void setViviendaActual(int idVivienda) {
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();
        if(sqLiteDatabase!=null){
            ContentValues contentValues = new ContentValues();
            // todas a false
            contentValues.put(PanicButtonBDContract.Vivienda.ACTUAL,0);
            sqLiteDatabase.update(PanicButtonBDContract.Vivienda.TABLE_NAME,contentValues,null,null);

            // actual
            contentValues.put(PanicButtonBDContract.Vivienda.ACTUAL,1);
            sqLiteDatabase.update(PanicButtonBDContract.Vivienda.TABLE_NAME,contentValues,PanicButtonBDContract.Vivienda.ID_VIVIENDA +" = "+idVivienda,null);

        }
    }

    public int getViviendaActual() {
        Cursor cursor = null;
        int idViviendaActual = -1;
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getReadableDatabase();
        if(sqLiteDatabase!=null){
            String[] projection = new String[]{
                    PanicButtonBDContract.Vivienda.ID_VIVIENDA,
            };

            cursor = sqLiteDatabase.query(PanicButtonBDContract.Vivienda.TABLE_NAME,
                    projection,
                    PanicButtonBDContract.Vivienda.ACTUAL+ " = ?",
                    new String[]{"1"},
                    null,
                    null,
                    null);
        }

        assert cursor != null;
        while (cursor.moveToNext()){
            idViviendaActual = cursor.getInt(0);
        }

        return idViviendaActual;
    }


    public void deleteViviendas() {
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();
        if(sqLiteDatabase!=null){
            sqLiteDatabase.delete(PanicButtonBDContract.Vivienda.TABLE_NAME,null,null);
        }
    }

    //endregion

    //region Localizacion

    public void insertLocalizacion(float lat, float lon, Timestamp timestamp) {
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();

        if(sqLiteDatabase !=null){
            ContentValues contentValues = new ContentValues();
            contentValues.put(PanicButtonBDContract.Location.LATITUD,lat);
            contentValues.put(PanicButtonBDContract.Location.LONGITUD,lon);
            contentValues.put(PanicButtonBDContract.Location.FECHA,timestamp.toString());
            sqLiteDatabase.insert(PanicButtonBDContract.Location.TABLE_NAME,null,contentValues);
        }

    }

    public Cursor getLocalizaciones(){

        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getReadableDatabase();
        if(sqLiteDatabase!=null){
            String[] projection = new String[]{
                    PanicButtonBDContract.Location.LATITUD,
                    PanicButtonBDContract.Location.LONGITUD,
                    PanicButtonBDContract.Location.FECHA
            };

            cursor = sqLiteDatabase.query(PanicButtonBDContract.Location.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null);
        }
        return cursor;
    }

    // borrar las localizaciones una vez se han subido a la base de datos
    public void deleteLocalizaciones(){
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getWritableDatabase();
        if(sqLiteDatabase!=null){
            sqLiteDatabase.delete(PanicButtonBDContract.Location.TABLE_NAME,null,null);
        }
    }

    /**
     * primero comprueba si existe, si existe si visto es diferente
     *
     * @param idAviso
     * @param visto
     * @return devolverá true si no existe o visto es diferente
     */
    public boolean esNuevo(int idAviso,int idAvisoPeriodo, boolean visto) {
        boolean valid = false;
        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = panicButtonBDHelper.getReadableDatabase();
        if(sqLiteDatabase!=null){
            String[] projection = new String[]{
                    PanicButtonBDContract.Avisos.ID_AVISO,
                    PanicButtonBDContract.Avisos.ID_AVISO_PERIOCIDAD,
                    PanicButtonBDContract.Avisos.VISTO
            };

            cursor = sqLiteDatabase.query(PanicButtonBDContract.Avisos.TABLE_NAME,
                    projection,
                    PanicButtonBDContract.Avisos.ID_AVISO + " = ? and "+PanicButtonBDContract.Avisos.ID_AVISO_PERIOCIDAD + " = ?",
                    new String[]{String.valueOf(idAviso),String.valueOf(idAvisoPeriodo)},
                    null,
                    null,
                    null);
        }
        if(cursor!=null && cursor.getCount()!=0){
            cursor.moveToNext();
            boolean vistoSqlite = cursor.getInt(2) == 1;
            if(visto != vistoSqlite){
                valid = true;
            }

        }else{
            valid = true;
        }



        return valid;
    }



    //endregion
}
