package dam.android.appproyecto.model

import dam.android.appproyecto.utils.Constants

class Aviso(var idAviso: Int, var idPeriocidad:Int,var titulo:String, var tipo:String, var descripcion:String, var fecha:String,var visto:Boolean){

   // el modo es el atributo para pintar
    var modo:String
    init {
        if(visto) {
            modo = Constants.CARD_TYPES.NOTIFICATION_DONE
        }else{
            modo = tipo
        }
    }

}