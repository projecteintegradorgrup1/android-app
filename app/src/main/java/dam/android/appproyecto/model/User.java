package dam.android.appproyecto.model;

import java.io.Serializable;

import dam.android.appproyecto.utils.Constants;

public class User implements Serializable {
    private int id;
    private String  genero;
    private String  fec_nacimiento;
    private String  nss;
    private String  nombre;
    private String  apellido1;
    private String  apellido2;
    private String  telefono;
    private String  email;
    private String  dni;
    private String error;
    private boolean isAsistente;
    /*
    select d.id,d.genero,d.fec_nacimiento,d.nss,p.nombre,p.apellido1,p.apellido2,p.telefono,p.email,p.dni from dependiente_table d \n" +
            "inner join persona_table p\n" +
            "on d.id_persona = p.id \n" +
            "where p.dni = ? and d.password=?

     */

    public User(int id, String genero, String fec_nacimiento, String nss, String nombre, String apellido1, String apellido2, String telefono, String email,String dni) {
        this.id = id;
        this.genero = genero;
        this.fec_nacimiento = fec_nacimiento;
        this.nss = nss;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.telefono = telefono;
        this.email = email;
        this.error = "";
        this.dni = dni;
    }

    public User(){
        error = "";
    }

    public boolean isAsistente() {
        return isAsistente;
    }

    public void setIsAsistente(boolean b){
        isAsistente = b;
    }

    public boolean error(){
        return error.equals(Constants.ERROR_LOGIN);
    }

    public int getId() {
        return id;
    }


    public String getNss() {
        return nss;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDni() {
        return dni;
    }

    public void setError() {
        this.error = Constants.ERROR_LOGIN;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", genero='" + genero + '\'' +
                ", fec_nacimiento='" + fec_nacimiento + '\'' +
                ", nss='" + nss + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", telefono='" + telefono + '\'' +
                ", email='" + email + '\'' +
                ", dni='" + dni + '\'' +
                '}';
    }
}
