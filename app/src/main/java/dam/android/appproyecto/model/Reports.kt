package dam.android.appproyecto.model

import java.io.Serializable

class Reports(var name: String?, var subtitle: String?, var image_drawable: Int, var urlDirectionPDF: String?, var urlDirectionHTML: String?, var hasParameters: Boolean) : Serializable {

    fun getDirectionPDF(): String {
        return urlDirectionPDF.toString()
    }

    fun getDirectionHTML(): String {
        return urlDirectionHTML.toString()
    }

    fun gethasParameters(): Boolean {
        return hasParameters
    }

    fun getNames(): String {
        return name.toString()
    }

    fun getsubtitle(): String {
        return subtitle.toString()
    }

    fun setNames(name: String) {
        this.name = name
    }

    fun getImage_drawables(): Int {
        return image_drawable
    }

    fun setImage_drawables(image_drawable: Int) {
        this.image_drawable = image_drawable
    }


}
