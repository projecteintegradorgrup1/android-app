package dam.android.appproyecto;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.AtomicFile;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.BindException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import dam.android.appproyecto.database.SQLite.PanicButtonBDContract;
import dam.android.appproyecto.database.SQLite.PanicButtonBDManager;
import dam.android.appproyecto.model.Aviso;
import dam.android.appproyecto.model.User;
import dam.android.appproyecto.model.Vivienda;
import dam.android.appproyecto.utils.Constants;

import static android.content.Context.MODE_PRIVATE;


/**
 * llamar a cualquier metodo del crud abre y cierra la conexion
 * <p>
 * metodos que hay que decirle que se ha modificado algo de la base de datos pero que puede no haber internet y solo ponerse en el sqlite (luego se resubiran cuando vuelva el internet)
 * setVistoNotificacion, setPosicion, setViviendaActual
 */
public class Controlador {

    private Connection conn;
    private static boolean conectividad = false;

    private synchronized void conectar() throws ClassNotFoundException, SQLException {
        Class.forName(Constants.POSTGRES_DRIVER);
        Properties props = new Properties();
        props.setProperty("user", Constants.BD_USER);
        props.setProperty("password", Constants.BD_PASSWORD);
        conn = DriverManager.getConnection(Constants.URL_POSTGRES + Constants.IP_POSTGRES + ":" + Constants.BD_PORT + "/" + Constants.BD_NAME, props);
    }

    private void close() throws SQLException {
        conn.close();
    }

    //region Login

    /**
     * @param dni      dni del usuario a logear
     * @param password contra del usuario a logear
     * @return el nombre del usuario si existe y su id, sino "-1" puesto en la clase constantes como Constantes.LOGIN_ERROR en la primera posicion
     * <p>
     * Coge el usuario y si existe setea sus viviendas en el sqlite
     * <p>
     *
     * user
     *
     * select user login prepared statement primer parametro dni, segundo contraseña
     * select d.id,d.genero,d.fec_nacimiento,d.nss,p.nombre,p.apellido1,p.apellido2,p.telefono,p.email,p.dni from dependiente_table d \n" +
     * "inner join persona_table p\n" +
     * "on d.id_persona = p.id \n" +
     * "where p.dni = ? and d.password=?
     *
     * asistente
     *
     * select * from asistente_table a inner join persona_table p\n" +
     *             "on a.id_persona = p.id where p.dni=? and a.password = ?
     *
     *
     */
    public User getLogin(String dni, String password, Context context) throws SQLException, ClassNotFoundException {
        User user = new User();
        boolean error = true;
        if (isConectividad()) {
            conectar();
            if (!conn.isClosed()) {


                // comprobar si es usuario
                PreparedStatement pstmt = conn.prepareStatement(Constants.SELECT_USER_LOGIN);
                pstmt.setString(1, dni.toUpperCase());
                pstmt.setString(2, password);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
                    user.setIsAsistente(false);
                    error = false;
                }

                // comprobar si es asistente
                // si ya ha entrado en el bucle error será false, entonces si no ha entrado comrpbar si es asistente
                if(error){
                    PreparedStatement pstmtAsistente = conn.prepareStatement(Constants.SELECT_ASISTANT_LOGIN);
                    pstmtAsistente.setString(1, dni.toUpperCase());
                    pstmtAsistente.setString(2, password);
                    ResultSet rsAsistente = pstmtAsistente.executeQuery();
                    while (rsAsistente.next()) {
                        user = new User();
                        user.setIsAsistente(true);
                        error = false;
                    }


                }

            }
        }
        if (error) {
            user.setError();
        } else {

            if(!user.isAsistente()){
                setViviendas(user.getId(), context);
            }

        }

        if (conn != null) {
            close();
        }

        return user;
    }
    //endregion
    //region Viviendas
    // en este metodo no hace falta abrir la conexion ni cerrarla porque se llama desde  de getLogin
    // coge las vivendas del dependiente cunado se logea

    // select v.id, d.tipo_via,d.direccion,d.num, ((select id_vivienda_actual from dependiente_table where id = ?)=v.id)
    //from vivienda_table v
    //inner join direccion_table d
    //on v.id_direccion = d.id
    //where id_dependiente = ?
    private void setViviendas(int idDependiente, Context context) throws SQLException, ClassNotFoundException {
        ArrayList<Vivienda> viviendas = new ArrayList<>();
        if (isConectividad()) {
            if(conn == null){
                conectar();
            }
            // coger vivienda actual
            PreparedStatement preparedStatementViviendas = conn.prepareStatement(Constants.GET_VIVIENDAS);

            // comprobar cual es la vivienda actual
            preparedStatementViviendas.setInt(1, idDependiente);
            preparedStatementViviendas.setInt(2, idDependiente);
            ResultSet resultSet = preparedStatementViviendas.executeQuery();


            while (resultSet.next()) {
                viviendas.add(new Vivienda(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getBoolean(5)));
            }


        }
        PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
        panicButtonBDManager.insertViviendas(viviendas);


    }

    // sentencia:
    // update dependiente_table
    //set id_vivienda_actual = ?
    //where id = ?
    public void setViviendaActual(Context context, int idVivienda, int idDependiente) throws SQLException, ClassNotFoundException {
        if (isConectividad()) {
            conectar();
            PreparedStatement preparedStatement = conn.prepareStatement(Constants.SET_VIVIENDA_ACTUAL);
            preparedStatement.setInt(1, idVivienda);
            preparedStatement.setInt(2, idDependiente);
            preparedStatement.execute();
            close();
        } else {

            modificadoOffline(context, Constants.MODIFICADO_OFFLINE_VIVIENDA);

        }

        PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
        panicButtonBDManager.setViviendaActual(idVivienda);

    }


    // borrar las actuales y volver a pedirlas. Por si el asistente lo ha modificado
    public ArrayList<Vivienda> getViviendas(int idDpenediente,Context context) throws SQLException, ClassNotFoundException {
        PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);

        if(isConectividad()) {

            panicButtonBDManager.deleteViviendas();
            setViviendas(idDpenediente, context);
        }


        Cursor cursor = panicButtonBDManager.getViviendas();

        ArrayList<Vivienda> viviendas = new ArrayList<>();

        while (cursor.moveToNext()) {
            boolean actual = cursor.getInt(4) == 1;
            viviendas.add(new Vivienda(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), actual));
        }

        return viviendas;
    }

    //endregion
    //region Notificaciones

    /**
     * Metodo que guarda las notificaciones del dia en el sqlite
     * si hay alguna fila, devolverá true
     */
    public boolean setNotificacionesToSqlite(int idDependiente, Context context) throws SQLException, ClassNotFoundException {
        boolean valid = false;
        if (isConectividad()) {
            conectar();

            PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
            PreparedStatement periodicidad = conn.prepareStatement(Constants.SELECT_NOTIFICACION_CON_PERIODICIDAD, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            periodicidad.setInt(1, idDependiente);

            ResultSet resultSet = periodicidad.executeQuery();

            valid = hayNuevasNotificiones(resultSet,panicButtonBDManager);
            panicButtonBDManager.deletAvisos();

            while (resultSet.next()) {

                Aviso a = new Aviso(
                        resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        formatearFecha(resultSet.getString(6)),
                        resultSet.getBoolean(7)
                );

                panicButtonBDManager.insert(a);
            }

            close();
        }
        return valid;
    }

    // devolverá true si hay algun campo diferente
    private boolean hayNuevasNotificiones(ResultSet resultSet, PanicButtonBDManager panicButtonBDManager) throws SQLException {
        boolean valid = false;
        while(resultSet.next()) {
            valid = panicButtonBDManager.esNuevo(resultSet.getInt(1),resultSet.getInt(2), resultSet.getBoolean(7));
        }

        resultSet.beforeFirst();
        return valid;
    }

    /**
     * @param fecha DD/MM/YYYY HH:MM:SS
     * @return HH:MM
     */
    public String formatearFecha(String fecha) {
        String temp = fecha.split(" ")[1];
        return temp.split(":")[0] + ":" + temp.split(":")[1];
    }


    /**
     * @return devuelve los avisos dependiendo del  tipo
     */
    public ArrayList<Aviso> getAvisos(Context context, String tipo) {
        PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
        ArrayList<Aviso> avisos = new ArrayList<>();
        Cursor c = panicButtonBDManager.getAvisos(tipo);
        // si hay registros
        if (c.moveToFirst()) {
            do {
                boolean visto = false;
                if (c.getInt(7) == 1) {
                    visto = true;
                }
                avisos.add(new Aviso(c.getInt(0),
                        c.getInt(1),
                        c.getString(2),
                        c.getString(3),
                        c.getString(5),
                        c.getString(6),
                        visto));
            } while (c.moveToNext());
        }

        return avisos;
    }

    // para coger los vistos o no de un tipo, metodo sobreescrito de getAvisos con filtro
    public ArrayList<Aviso> getAvisos(Context context, String tipo,boolean vistos) {
        PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
        ArrayList<Aviso> avisos = new ArrayList<>();
        Cursor c = panicButtonBDManager.getAvisos(tipo, vistos);
        // si hay registros
        if (c.moveToFirst()) {
            do {
                boolean visto = false;
                if (c.getInt(7) == 1) {
                    visto = true;
                }
                avisos.add(new Aviso(c.getInt(0),
                        c.getInt(1),
                        c.getString(2),
                        c.getString(3),
                        c.getString(5),
                        c.getString(6),
                        visto));
            } while (c.moveToNext());
        }

        return avisos;
    }


    public void setVistoNotificacion(Aviso aviso, Context context) {
        if (!aviso.getVisto()) {
            if (isConectividad()) {

                // bd
                try {
                    conectar();
                    PreparedStatement preparedStatement = conn.prepareStatement(Constants.SET_AVISO_VISTO);
                    preparedStatement.setInt(1, aviso.getIdPeriocidad());
                    preparedStatement.execute();
                    close();
                } catch (ClassNotFoundException | SQLException e) {
                    e.printStackTrace();
                }

            } else {
                modificadoOffline(context, Constants.MODIFICADO_OFFLINE_NOTIFICACIONES);
            }
            // sqlite
            PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
            panicButtonBDManager.setVistoAviso(aviso);

        }
    }


    //endregion
    //region localizacion
    // consulta:
    // insert into geolocalizacion_table (id_dependiente, latitud, longitud, fecha_hora) values(?,?,?,?)
    public void setLocalizacion(Context context, int idDependiente, double lat, double lon) throws SQLException, ClassNotFoundException {

        if (isConectividad()) {
            // bd
            conectar();
            PreparedStatement preparedStatement = conn.prepareStatement(Constants.SET_LOCATION);
            preparedStatement.setInt(1, idDependiente);
            preparedStatement.setFloat(2, (float) lat);
            preparedStatement.setFloat(3, (float) lon);
            preparedStatement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            preparedStatement.execute();
            close();

        } else {
            modificadoOffline(context, Constants.MODIFICADO_OFFLINE_LOCALIZACION);
            PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
            panicButtonBDManager.insertLocalizacion((float) lat, (float) lon, new Timestamp(System.currentTimeMillis()));

        }
    }
    //endregion

    //region Offline

    // metodo que se llama cuando el broadcast receiver detecte que hay conexion
    public void hayConexion(Context context) {
        setConectividad(true);
        new ThreadActualizar(context).start();

    }
    // se llamará cuando vuelva la conexion subir las localizaciones, la vivienda actual y los vistos
    private void actualizarBDSqliteCuandoConexion(Context context, boolean localizacion, boolean vivienda, boolean notificaciones) throws SQLException, ClassNotFoundException {
        conectar();
        // necesitamos el id del dependiente en todas las opciones
        int idDependiente = context.getSharedPreferences(Constants.Preferences.FILE_NAME, MODE_PRIVATE).getInt(Constants.Preferences.LOGIN_ID, -1);
        if (vivienda) {
            // coger el dependiente
            int idViviendaActual = new PanicButtonBDManager(context).getViviendaActual();

            PreparedStatement preparedStatement = conn.prepareStatement(Constants.SET_VIVIENDA_ACTUAL);
            preparedStatement.setInt(1, idViviendaActual);
            preparedStatement.setInt(2, idDependiente);
            preparedStatement.execute();

        }
        if (notificaciones) {
            Cursor cursor = new PanicButtonBDManager(context).getAvisosVisto();
            // poner todas en modo lote
            try {
                PreparedStatement preparedStatement = conn.prepareStatement(Constants.SET_AVISO_VISTO);
                while (cursor.moveToNext()) {
                    preparedStatement.setInt(1, cursor.getInt(0));
                    preparedStatement.addBatch();

                }
                preparedStatement.executeBatch();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        if (localizacion) {
            PanicButtonBDManager panicButtonBDManager = new PanicButtonBDManager(context);
            //get localizaciones
            Cursor cursor = panicButtonBDManager.getLocalizaciones();
            // poner todas en modo lote
            try {
                int id = context.getSharedPreferences(Constants.Preferences.FILE_NAME, Context.MODE_PRIVATE).getInt(Constants.Preferences.LOGIN_ID,-1);
                PreparedStatement preparedStatement = conn.prepareStatement(Constants.SET_LOCATION);
                while (cursor.moveToNext()) {
                    preparedStatement.setInt(1, id);
                    preparedStatement.setFloat(2, cursor.getFloat(0));
                    preparedStatement.setFloat(3, cursor.getInt(1));
                    preparedStatement.setTimestamp(4,  Timestamp.valueOf(cursor.getString(2)));
                    preparedStatement.addBatch();

                }
                preparedStatement.executeBatch();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            // delete las que hay en el sqlite para no acumularlas
            panicButtonBDManager.deleteLocalizaciones();
        }
        close();
    }


    // pone a true en el preferences el valor que se ha modificado sin internet
    private void modificadoOffline(Context context, String datoModificado) {
        SharedPreferences myPreferences = context.getSharedPreferences(Constants.Preferences.FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreferences.edit();
        switch (datoModificado) {
            case Constants.MODIFICADO_OFFLINE_VIVIENDA:
                editor.putBoolean(Constants.MODIFICADO_OFFLINE_VIVIENDA, true);
                break;
            case Constants.MODIFICADO_OFFLINE_NOTIFICACIONES:
                editor.putBoolean(Constants.MODIFICADO_OFFLINE_NOTIFICACIONES, true);
                break;
            case Constants.MODIFICADO_OFFLINE_LOCALIZACION:
                editor.putBoolean(Constants.MODIFICADO_OFFLINE_LOCALIZACION, true);
                break;

        }
        editor.apply();

    }


    // una vez se ha actualizado poner otra vez todos los valores a false
    private void ponerAFalseValoresOffline(Context context) {
        SharedPreferences myPreferences = context.getSharedPreferences(Constants.Preferences.FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreferences.edit();
        editor.putBoolean(Constants.MODIFICADO_OFFLINE_VIVIENDA, false);
        editor.putBoolean(Constants.MODIFICADO_OFFLINE_NOTIFICACIONES, false);
        editor.putBoolean(Constants.MODIFICADO_OFFLINE_LOCALIZACION, false);
        editor.apply();

    }

    public void noHayConexion() {
        setConectividad(false);
    }


    class ThreadActualizar extends Thread {

        private Context context;

        ThreadActualizar(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            SharedPreferences myPreferences = context.getSharedPreferences(Constants.Preferences.FILE_NAME, MODE_PRIVATE);
            // coger los tres booleanos para saber que se ha modificado
            boolean modificadoOfflineVivienda = myPreferences.getBoolean(Constants.MODIFICADO_OFFLINE_VIVIENDA, false);
            boolean modificadoOfflineLocalizacion = myPreferences.getBoolean(Constants.MODIFICADO_OFFLINE_LOCALIZACION, false);
            boolean modificadoOfflineNotificaciones = myPreferences.getBoolean(Constants.MODIFICADO_OFFLINE_NOTIFICACIONES, false);

            if (modificadoOfflineVivienda || modificadoOfflineLocalizacion || modificadoOfflineNotificaciones) {
                try {
                    actualizarBDSqliteCuandoConexion(context, modificadoOfflineLocalizacion, modificadoOfflineVivienda, modificadoOfflineNotificaciones);
                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();

                }
                ponerAFalseValoresOffline(context);
            }
        }
    }
    //endregion

    public static synchronized boolean isConectividad() {
        return conectividad;
    }

    private static synchronized void setConectividad(Boolean b) {
        conectividad = b;
    }

    public boolean aviso(String maquina, String dni) {
        boolean valid = false;
        if (validIP(maquina)) {
            valid = peticion(maquina, dni);
        }
        if(!valid) {

            try {
                ArrayList<String> ips = getIpAviable();

                for (String ip : ips) {
                    if (valid = peticion(ip, dni)) {
                        break;
                    }
                }

            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
        return valid;
    }


    private static boolean validIP (String ip) {
        try {
            if ( ip == null || ip.isEmpty() ) {
                return false;
            }

            String[] parts = ip.split( "\\." );
            if ( parts.length != 4 ) {
                return false;
            }

            for ( String s : parts ) {
                int i = Integer.parseInt( s );
                if ( (i < 0) || (i > 255) ) {
                    return false;
                }
            }
            if ( ip.endsWith(".") ) {
                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    // si le han cogido la alarma devuelve true
    private boolean peticion(String maquina, String dni) {
        boolean valid = false;
        Socket socket;
        int puerto = 2020;
        try {

            socket = new Socket();
            // timeout de 5 segundos
            socket.setSoTimeout(2000);
            InetSocketAddress socketAddress = new InetSocketAddress(InetAddress.getByName(maquina), puerto);

            socket.connect(socketAddress);
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                 BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()))
            ) {
                bw.write(dni);
                bw.newLine();
                bw.flush();
                String read = br.readLine();

                if (read.equals("OK")) {
                    valid = true;
                }
            }
        }catch (ConnectException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return valid;
    }


    private ArrayList<String> getIpAviable() throws SQLException, ClassNotFoundException {
        conectar();
        ArrayList<String> ips = new ArrayList<>();
        ResultSet resultSet = conn.createStatement().executeQuery(Constants.GET_IPS_AVAIABLE);
        while (resultSet.next()) {
            ips.add(resultSet.getString(1));
        }


        close();
        return ips;
    }

}
