package dam.android.appproyecto.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;

import dam.android.appproyecto.Activities.BotonActivity;
import dam.android.appproyecto.R;

public class NotificationUtil extends ContextWrapper {

    private NotificationManager mManager;
    public static final String CHANNEL_ID = "dam.android.appproyecto.avisos";
    public static final String CHANNEL_NAME = "AVISOS_CHANNEL";



    public NotificationUtil(Context base) {
        super(base);
        createChannels();
    }


    public void createChannels() {
        // create channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // el nivel alto de importancia alto vibra y se asoma
            // el por defecto no se asoma

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);

            // propiedades
            channel.enableVibration(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            // crear el canal
            getManager().createNotificationChannel(channel);

        }



    }

    public NotificationManager getManager(){
        if(mManager==null){
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return mManager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getChannelNotification(String title, String body) {
        // pending intent para cuando clicke la notificacion
        // Creamos el Intent que llamará a nuestra Activity
        Intent targetIntent = new Intent(getBaseContext(),
                BotonActivity.class);
        // Creamos el PendingIntent
        PendingIntent contentIntent = PendingIntent.getActivity(
                getBaseContext(), 0, targetIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);


        /* TODO PONER ICONO DE ANDROID (CORAZÓN) A LA NOTIFICACIÖN */
        return new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                .setContentTitle(getString(R.string.recibirNotificacion))
                .setColor(getColor(R.color.background))
                .setSubText(getString(R.string.subTitleNotificacion))
                .setSmallIcon(R.drawable.icon1)
                .setLargeIcon(BitmapFactory.decodeResource(getBaseContext().getResources(),R.drawable.icon1))
                .setAutoCancel(true)
                .setContentIntent(contentIntent);
    }


}
