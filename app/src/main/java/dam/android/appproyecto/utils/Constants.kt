package dam.android.appproyecto.utils





object Constants{

    // Key intents
    const val KEY_LOGIN_USER = "user"

    object CARD_TYPES{
        const val NOTIFICATION_DONE = "notificationDone"
        const val NOTIFICATION = "Notificacion"
        const val MEDICAMENT = "Medicamento"
    }


    // constantes base de datos
    // 10.0.2.2 cuan el tunel, sino 149.202.8.235 y el port 5432, en localhost 9999
    const val URL_POSTGRES = "jdbc:postgresql://"
    const val IP_POSTGRES = "149.202.8.235"
    const val BD_PORT = "5432"
    const val BD_NAME = "bdgrup1"
    const val BD_PASSWORD = "Grupo-427"
    const val BD_USER = "grup1"
    const val POSTGRES_DRIVER = "org.postgresql.Driver"

    // constantes sentencias sql
    const val SELECT_USER_LOGIN = "select d.id,d.genero,d.fec_nacimiento,d.nss,p.nombre,p.apellido1,p.apellido2,p.telefono,p.email,p.dni from dependiente_table d \n" +
            "inner join persona_table p\n" +
            "on d.id_persona = p.id \n" +
            "where p.dni = ? and d.password=?"

    const val SELECT_ASISTANT_LOGIN = "select * from asistente_table a inner join persona_table p\n" +
            "on a.id_persona = p.id where p.dni=? and a.password = ?"


    const val SELECT_NOTIFICACION_CON_PERIODICIDAD = "select a.id,ap.id,a.descripcion,a.tipo,ap.descripcion,ap.hora_fecha,ap.visto\n" +
            "from aviso_table a \n" +
            "inner join aviso_periodicidad_table ap\n" +
            "on a.id = ap.aviso_id \n" +
            "where  a.dependiente_id = ? \n" +
            "and (current_date >= a.fec_desde)\n" +
            "and (a.tipo = 'Notificacion' or a.tipo = 'Medicamento')"

    const val SET_AVISO_VISTO = "update aviso_periodicidad_table\n" +
            "set visto = true\n" +
            "where id = ?"

    const val GET_VIVIENDAS = "select v.id, d.tipo_via,d.direccion,d.num, ((select id_vivienda_actual from dependiente_table where id = ?)=v.id)\n" +
            "from vivienda_table v \n" +
            "inner join direccion_table d \n" +
            "on v.id_direccion = d.id \n" +
            "where id_dependiente = ?"

    const val SET_VIVIENDA_ACTUAL = "update dependiente_table \n" +
            "set id_vivienda_actual = ?\n" +
            "where id = ?"

    const val SET_LOCATION = "insert into geolocalizacion_table (dependiente_id, latitud, longitud, fecha_hora) values(?,?,?,?)"
    const val GET_IPS_AVAIABLE = "select ip from guarda_ip_table"

    // constante error
    const val ERROR_LOGIN = "-1"

    // constantes del archivo preferences
    object Preferences{
        const val FILE_NAME = "myPreferences"

        // valores del usuario
        const val LOGIN_DNI = "loginDni"
        const val LOGIN_CONTRA = "loginContra"
        const val LOGIN_ID = "id"
        const val LOGIN_USER = "nombre"
        const val LOGIN_APELLIDO1 = "apellido1"
        const val LOGIN_APELLIDO2 = "apellido2"
        const val LOGIN_EMAIL = "email"
        const val LOGIN_TELEFONO = "telefono"
        const val LOGIN_NSS = "nss"


        const val LOGIN_ASISTENTE = "asistente"

        const val FIRST_TIME = "firstTime"
    }



    // constantes de ajustes
    object Ajustes{
        const val RESULT_CODE = 1
        const val NIGHT_MODE = "night_mode"
        const val SYNC_NOTIFICATION = "sync_frequency_notification"
        const val SYNC_NOTIFICATION_VALUE = "sync_frequency_notification_value"
        const val SYNC_NOTIFICATION_DEFAULT = "3 horas"
        const val SYNC_LOCATION = "sync_frequency_location"
        const val SYNC_LOCATION_VALUE = "sync_frequency_location_value"
        const val SYNC_LOCATION_DEFAULT = "15 minutos"
        const val IP = "ip_teleoperador"
    }


    // modo offline
    const val MODIFICADO_OFFLINE_VIVIENDA = "offlineVivienda"
    const val MODIFICADO_OFFLINE_LOCALIZACION = "offlineLocalizacion"
    const val MODIFICADO_OFFLINE_NOTIFICACIONES = "offlineNotificaciones"



    // vivienda result
    const val RESULT_CODE_VIVIENDA = 0
    const val RESULT_VIVIENDA = "resultVivienda"
    const val RESULT_DEPENDIENTE = "resultDependiente"




}