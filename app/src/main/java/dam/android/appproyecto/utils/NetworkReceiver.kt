package dam.android.appproyecto.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import dam.android.appproyecto.Controlador

class NetworkStateReceiver: BroadcastReceiver(){

    // tambien se llama cuando pasa por el on resume
    override fun onReceive(context: Context, intent: Intent) {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ni = manager.activeNetworkInfo
        onNetworkChange(ni,context)
    }

    fun onNetworkChange(networkInfo: NetworkInfo?, context: Context) {
        if (networkInfo != null) {
            if (networkInfo.isConnected) {
                Controlador().hayConexion(context)
            } else{
                Controlador().noHayConexion()
            }
        }else{
            Controlador().noHayConexion()
        }
    }


}