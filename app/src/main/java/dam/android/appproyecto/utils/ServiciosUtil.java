package dam.android.appproyecto.utils;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.util.Log;

import dam.android.appproyecto.Service.AvisosServicio;
import dam.android.appproyecto.Service.LocationService;

public class ServiciosUtil {


    private static  int perido_ms_notification;
    private static  int perido_ms_location;

    public static void scheduleJobLocation(Context context) {
        Log.i("ALGO","LOCATION");
        // localizacion
        ComponentName serviceComponentLocation = new ComponentName(context,LocationService.class);
        JobInfo.Builder builderLocation = new JobInfo.Builder(1,serviceComponentLocation);
        checkSynLocation(context);
        builderLocation.setPeriodic(perido_ms_location);
        builderLocation.setPersisted(true);

        JobScheduler jobSchedulerLocation = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobSchedulerLocation.schedule(builderLocation.build());

    }


    private static void checkSynLocation(Context context){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String perido = myPreferences.getString(Constants.Ajustes.SYNC_LOCATION_VALUE,"900000");
        // el valor esta en miutos, pasar a milisegundos
        perido_ms_location = Integer.parseInt(perido);//* 60 * 1000;
    }




    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void scheduleJobNotificacion(Context context) {
        Log.i("ALGO","NOTIFICATION");
        // notificaciones
        ComponentName serviceComponent = new ComponentName(context,AvisosServicio.class);
        JobInfo.Builder builder = new JobInfo.Builder(0,serviceComponent);
        checkSyncNotification(context);
        builder.setPeriodic(perido_ms_notification);
        builder.setPersisted(true);

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(builder.build());

    }




    // comprobar el tiempo de periodicidad
    private static void checkSyncNotification(Context context){

        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String perido = myPreferences.getString(Constants.Ajustes.SYNC_NOTIFICATION_VALUE,"3600000");
        // el valor esta en miutos, pasar a milisegundos
        perido_ms_notification = Integer.parseInt(perido);


    }

}
