package dam.android.appproyecto.Service;


import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;

import dam.android.appproyecto.utils.Constants;


/**
 * El boot receiver reiniciará el servicio si el movil se apaga y se vuelve a encender
 */
public class BootReceiver extends BroadcastReceiver {


    private static  int perido_ms_notification;


    @Override
    public void onReceive(Context context, Intent intent) {
        //scheduleJob(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void scheduleJob(Context context) {
        // notificaciones
        ComponentName serviceComponent = new ComponentName(context,AvisosServicio.class);
        JobInfo.Builder builder = new JobInfo.Builder(0,serviceComponent);
        checkSyncNotification(context);

        builder.setMinimumLatency(perido_ms_notification);
        builder.setOverrideDeadline(perido_ms_notification);

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(builder.build());

    }




    // comprobar el tiempo de periodicidad
    private static void checkSyncNotification(Context context){

        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String perido = myPreferences.getString(Constants.Ajustes.SYNC_NOTIFICATION_VALUE,"180");
        // el valor esta en miutos, pasar a milisegundos
        perido_ms_notification = Integer.parseInt(perido) * 60 * 1000;


    }

}
