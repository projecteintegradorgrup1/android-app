package dam.android.appproyecto.Service;


import android.app.Notification;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Log;
import java.sql.SQLException;

import dam.android.appproyecto.utils.Constants;
import dam.android.appproyecto.utils.NotificationUtil;
import dam.android.appproyecto.Controlador;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class AvisosServicio extends JobService {



    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i("MALGO","on start notification");
        Handler mHandler = new Handler(getMainLooper());
        mHandler.post(new Runnable(){
           @RequiresApi(api = Build.VERSION_CODES.O)
           @Override
           public void run(){
               SharedPreferences myPreferences = getApplicationContext().getSharedPreferences(Constants.Preferences.FILE_NAME, MODE_PRIVATE);
               int id = (myPreferences.getInt(Constants.Preferences.LOGIN_ID, 0));
               try {

                   if(Controlador.isConectividad()){
                       boolean hayNotificaciones = new Controlador().setNotificacionesToSqlite(id,getApplicationContext());
                       if(hayNotificaciones) {
                           NotificationUtil notificationUtil = new NotificationUtil(getApplication());
                           Notification.Builder nb = notificationUtil.getChannelNotification("", "");
                           notificationUtil.getManager().notify(101, nb.build());
                       }
                   }


               } catch (SQLException | ClassNotFoundException e) {
                   Log.e("SQLERROR",e.getMessage());
               }

               jobFinished(params,true);


            }
        });
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) { return false; }
}