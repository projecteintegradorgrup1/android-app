package dam.android.appproyecto.Service;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import dam.android.appproyecto.utils.Constants;

public class BootReceiverLocation extends BroadcastReceiver {


    private static  int perido_ms_location;
    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.i("ON RECEIVE","LOCATION");
        //scheduleJob(context);
    }

    public static void scheduleJob(Context context) {
        // localizacion
        ComponentName serviceComponentLocation = new ComponentName(context,LocationService.class);
        JobInfo.Builder builderLocation = new JobInfo.Builder(1,serviceComponentLocation);
        checkSynLocationNotification(context);

        builderLocation.setMinimumLatency(perido_ms_location);
        builderLocation.setOverrideDeadline(perido_ms_location);
        builderLocation.setPersisted(true);

        JobScheduler jobSchedulerLocation = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobSchedulerLocation.schedule(builderLocation.build());

    }


    private static void checkSynLocationNotification(Context context){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String perido = myPreferences.getString(Constants.Ajustes.SYNC_LOCATION_VALUE,"5");
        // el valor esta en miutos, pasar a milisegundos
        perido_ms_location = Integer.parseInt(perido)*100;//* 60 * 1000;
    }
}
