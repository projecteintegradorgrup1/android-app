package dam.android.appproyecto.Service;

import android.Manifest;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.sql.SQLException;

import dam.android.appproyecto.Controlador;
import dam.android.appproyecto.utils.Constants;

public class LocationService extends JobService {

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i("ALGO","ON START LOCATION");
        FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Log.i("ALGO","ON LOCATION CALL BACK");
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Location location = locationResult.getLastLocation();
                    if (location != null) {
                        // coger las coordenadas
                        double latitude = location.getLatitude();
                        double longitud = location.getLongitude();
                        // dependiente
                        int idDependiente = getApplicationContext().getSharedPreferences(Constants.Preferences.FILE_NAME, MODE_PRIVATE).getInt(Constants.Preferences.LOGIN_ID, -1);


                        new Handler(getMainLooper()).post(() -> {
                            try {
                                new Controlador().setLocalizacion(getApplicationContext(), idDependiente, latitude, longitud);
                                jobFinished(params, true);
                            } catch (SQLException | ClassNotFoundException e) {
                                e.printStackTrace();
                            }


                        });


                    }


                }

            }
        };


        mFusedLocationProviderClient.requestLocationUpdates(getLocationRequest(), locationCallback, null /* Looper */);


        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(checkSynLocation(this));
        locationRequest.setFastestInterval(500);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    private int checkSynLocation(Context context){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String perido = myPreferences.getString(Constants.Ajustes.SYNC_LOCATION_VALUE,"900000");
        // el valor esta en miutos, pasar a milisegundos
        return Integer.parseInt(perido);//* 60 * 1000;
    }



}
