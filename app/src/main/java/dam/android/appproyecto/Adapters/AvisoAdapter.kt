package dam.android.appproyecto.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dam.android.appproyecto.R
import kotlinx.android.synthetic.main.card_item.view.*
import dam.android.appproyecto.model.Aviso


/**
 * el val listener es para sobreescribir el click, el listener recibe el aviso y el view asociado
 */
class AvisoAdapter(val items:ArrayList<Aviso>, val context: Context, val listener: (Aviso, View) -> Unit): RecyclerView.Adapter<AvisoAdapter.ViewHolder>(){


    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        holder.card?.setValues(items.get(position))

    }




    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.card_item, parent, false))
    }

    override fun getItemCount(): Int {
        return  items.size
    }


    fun isEmpty():Boolean{
        return items.isEmpty()
    }

    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        init {
            view.card.setOnClickListener {
                listener(items[adapterPosition],it)
            }
        }
        val card = view.card





    }


}

