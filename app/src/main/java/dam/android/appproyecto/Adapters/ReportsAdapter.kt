package dam.android.appproyecto.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import dam.android.appproyecto.R
import dam.android.appproyecto.model.Reports

class ReportsAdapter(ctx: Context, private val items: ArrayList<Reports>, val listener : (Reports)-> Unit) : RecyclerView.Adapter<ReportsAdapter.MyViewHolder>() {


    private val inflater: LayoutInflater


    init {

        inflater = LayoutInflater.from(ctx)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = inflater.inflate(R.layout.rv_item, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.iv.setImageResource(items[position].getImage_drawables())
        holder.title.setText(items[position].getNames())
        holder.subtitle.setText(items[position].getsubtitle())
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView
        var subtitle: TextView
        var iv: ImageView

        init {
            itemView.setOnClickListener { listener(items[adapterPosition]) }
            title = itemView.findViewById(R.id.tv) as TextView
            subtitle = itemView.findViewById(R.id.tvSubtitulo) as TextView
            iv = itemView.findViewById(R.id.iv) as ImageView
        }

    }
}
