package dam.android.appproyecto


import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import android.support.test.espresso.action.TypeTextAction
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import dam.android.appproyecto.Activities.Login

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule
import android.support.test.espresso.matcher.RootMatchers.isPlatformPopup
import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.v7.widget.RecyclerView
import android.view.View
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Rule
    @JvmField
    var myActivityTestRule = ActivityTestRule(Login::class.java)


    @Test
    fun loginCorrectoDependiente() {
        // escribir en los edit texts  bien los credenciales, cerramos cada vez el teclado para que pueda usar los otros elementos
        onView(withId(R.id.etDni)).perform(typeText("123a"), closeSoftKeyboard())
        onView(withId(R.id.etContra)).perform(typeText("123"), closeSoftKeyboard())

        //click en el boton de login
        onView(withId(R.id.btnLogin)).perform(click())

        // comprobar que estamos en la activity del boton
        onView(withId(R.id.button)).check(matches(withContentDescription("INSTRUMENTAL_TEST")))


    }

    @Test
    fun loginIncorrectoDependiente() {
        // escribir en los edit texts  mal los credenciales
        onView(withId(R.id.etDni)).perform(TypeTextAction("12345678a"), closeSoftKeyboard())
        onView(withId(R.id.etContra)).perform(TypeTextAction("12"), closeSoftKeyboard())

        //click en el boton de login
        onView(withId(R.id.btnLogin)).perform(click())


        // comprobar que no estamos en la activity del boton. Si da error al boton de login se le pondrá un content description
        onView(withId(R.id.btnLogin)).check(matches(not(withContentDescription("INSTRUMENTAL_TEST"))))


    }

    @Test
    fun modoNoche() {
        // escribir en los edit texts  mal los credenciales
        onView(withId(R.id.etDni)).perform(TypeTextAction("123a"), closeSoftKeyboard())
        onView(withId(R.id.etContra)).perform(TypeTextAction("123"), closeSoftKeyboard())

        //click en el boton de login
        onView(withId(R.id.btnLogin)).perform(click())

        // abrimos el popup menu
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext())
        onData(hasToString(`is`( myActivityTestRule.activity.getString(R.string.action_settings) ))) .inRoot(isPlatformPopup()).perform(click())

        // pulsamos el ajuste de modo noche
        onView(withId(R.id.settTheme)).perform(click())


        //comprobamos el modo noche
        onView(withId(R.id.titleDayNight)).check(matches(not(withContentDescription("NOCHE"))))


    }
    @Test
    fun modoDia() {
        // escribir en los edit texts  mal los credenciales
        onView(withId(R.id.etDni)).perform(TypeTextAction("123a"), closeSoftKeyboard())
        onView(withId(R.id.etContra)).perform(TypeTextAction("123"), closeSoftKeyboard())

        //click en el boton de login
        onView(withId(R.id.btnLogin)).perform(click())

        // abrimos el popup menu
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext())
        onData(hasToString(`is`( myActivityTestRule.activity.getString(R.string.action_settings) ))) .inRoot(isPlatformPopup()).perform(click())

        // pulsamos el ajuste de modo noche dos veces para que este en modo dia
        onView(withId(R.id.settTheme)).perform(click())
        onView(withId(R.id.settTheme)).perform(click())


        //comprobamos el modo noche
        onView(withId(R.id.titleDayNight)).check(matches(not(withContentDescription("DIA"))))


    }

    // solo irá si hay un elemento
    @Test
    fun verSiHayNotificacion(){
        // escribir en los edit texts  mal los credenciales
        onView(withId(R.id.etDni)).perform(TypeTextAction("123a"), closeSoftKeyboard())
        onView(withId(R.id.etContra)).perform(TypeTextAction("123"), closeSoftKeyboard())

        //click en el boton de login
        onView(withId(R.id.btnLogin)).perform(click())
        onView(withId(R.id.action_notification)).perform(click())


        onView(withId(R.id.rv_notifications)).check(matches(hasChildCount(1)))
    }



}
