package dam.android.appproyecto

import dam.android.appproyecto.model.User
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {


    // FORMATEO DE FECHA
    @Test
    fun formatearFecha() {
        val controlador = Controlador()
        val fechaEnviada = "20/02/2018 08:00:32"
        val fechaExpected = "08:00"
        val fechaResultado = controlador.formatearFecha(fechaEnviada)

        assertEquals(fechaExpected, fechaResultado)
    }


    // USUARIO CON ERROR O NO
    @Test
    fun userLogeadoConError(){
        val user = User()
        user.setError()
        assertEquals(true,user.error())
    }
    @Test
    fun userLogeadoSinError(){
        val user = User()
        assertEquals(false,user.error())
    }

}
